#ifndef TWL_WIDGET_H
#define TWL_WIDGET_H

  // TWC
#include <twc/wayland_types.h>      // wld_coordinate_t
#include <twc/twl_pixel_types.h>    // twl_color_t, twl_pixel_t

  // aa is for alpha, rr is for red, gg for is green, bb is for blue
  //                           aarrggbb
#define RED    ((twl_color_t)0xFFFF0000)
#define GREEN  ((twl_color_t)0xFF00FF00)
#define BLUE   ((twl_color_t)0xFF0000FF)
#define WHITE  ((twl_color_t)0xFFFFFFFF)
#define BLACK  ((twl_color_t)0xFF000000)
#define TRANSPARENT_BLACK ((twl_color_t)0x00000000)

#define MEDIUM_GRAY ((twl_color_t)0xFFBEBEBE)

#define GRAY_A0 ((twl_color_t)0xFFA0A0A0)
#define GRAY_40 ((twl_color_t)0xFF404040)
#define GRAY_26 ((twl_color_t)0xFF262626)

#define GRAY_BF ((twl_color_t)0xFFBFBFBF)
#define GRAY_7F ((twl_color_t)0xFF7F7F7F)
#define GRAY_4C ((twl_color_t)0xFF4C4C4C)

  /*
  ** A grid is a 2D array of panels.  Each panel is separated by
  ** PANEL_SEPARATOR pixels.
  **
  **        4 x 4 grid
  **
  **  panel width      = 10
  **  panel height     =  4
  **  panel separation =  2
  **
  **  * = panel field
  **  ||,  ++, --, represents separation
  **
  **
  **              1234567890
  **
  **  **********||********** 1
  **  **********||********** 2
  **  **********||********** 3
  **  **********||********** 4
  **  ----------++----------
  **  ----------++----------
  **  **********||X*********
  **  **********||**********
  **  **********||**********
  **  **********||**********
  **            12
  **
  ** The row and column indices for each panel in the grid form a
  ** panel coordinate system.  The 'X' is the origin pixel in panel
  ** (1,1).
  **
  ** Each dimension of a grid (in pixels) is given by the macro
  **
  **   TWC_WIDGET_GRID_DIMENSION(panel_count, panel_size)
  */

#define PANEL_SEPARATION 2

  // co is a panel-relative coordinate, ie, a row or column index.
  // di is the corresponding panel dimension in pixels.
#define TWC_WIDGET_PANEL_ORIGIN(co, di) ( (co) * ((di)+PANEL_SEPARATION) )

  // count is the total number of rows or columns
#define TWC_WIDGET_GRID_DIMENSION(count, di) (            \
    TWC_WIDGET_PANEL_ORIGIN(count, di) - PANEL_SEPARATION \
)

struct twl_grid {
    int row_count;  // in panels
    int col_count;  //   "

    wld_dimension_t panel_width;    // in pixels
    wld_dimension_t panel_height;   //   "

    wld_dimension_t stride; // of memory buffer in pixels
};

enum twl_button_state{
        PRESSED,
    NOT_PRESSED,
    NONE,
};

extern
void
twl_widget_draw_button(
    twl_pixel_t          *pix_memory,
    struct twl_grid      *twl_button_grid,
    int                   row,
    int                   col,
    enum twl_button_state button_state,
    const char           *text
);

extern
void
twl_widget_fill_pix_buf_all(
    twl_pixel_t    *pix_buf,
    wld_dimension_t W,          // buffer width
    wld_dimension_t H,          // buffer height
    twl_color_t     color
);

extern
void
twl_widget_fill_box(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  w,         // fill width
    wld_dimension_t  h,         // fill height
    wld_dimension_t  S,         // stride in pixels
    twl_color_t      color,
    wld_coordinate_t x0,        // origin of box
    wld_coordinate_t y0         //   "
);

enum twl_pattern {
    SOLID,
    STIPPLE,
};

extern
void
twl_widget_draw_box(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  width,
    wld_dimension_t  height,
    uint32_t         pixel_stride,  // stride in pixels
    twl_color_t      color,
    enum twl_pattern pattern
);

enum font_size {
    FS_NULL,
    FS_fixed_6x10,
    FS_fixed_7x12,
    FS_fixed_8x13,
    FS_fixed_9x15,
};

extern
wld_dimension_t
twl_widget_render_text(
    enum font_size   font_size,
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,          // buffer width
    wld_dimension_t  H,          // buffer height
    const char      *string,
    wld_coordinate_t x0,         // location within
    wld_coordinate_t y0,         // buffer
    twl_color_t      foreground,
    twl_color_t      background
);

  // Only the least significant 8 bits are used.
  // The bottom 4 are hotspot_y, the other 4 hotspot_x.
typedef uint32_t XC_hotspot_t;
#define XC_HOTSPOT_y(i) ( (XC_hotspot_t)  ((i) & 0x0f)     )
#define XC_HOTSPOT_x(i) ( (XC_hotspot_t) (((i) & 0xf0) >> 4))

#define XC_HOTSPOT_INVALID ((XC_hotspot_t) 0xffffffff)

#define CURSOR_GLYPH_WIDTH  16
#define CURSOR_GLYPH_HEIGHT 16

#define CURSOR_BYTES_PER_PIXEL 4

#define XC_wayland_logo XC_num_glyphs

extern
XC_hotspot_t
twl_widget_render_X_cursor(
    twl_pixel_t *pix_buf,
    int          cursor_index,
    twl_color_t  foreground,
    twl_color_t  background
);

#endif // TWL_WIDGET_H

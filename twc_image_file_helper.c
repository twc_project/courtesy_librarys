
  // System
#include <stdio.h>      // fopen()
#include <stddef.h>     // offsetof()
#include <string.h>     // memcpy
#include <setjmp.h>
#include <stdbool.h>

  // TWC
#include <twc/wayland_types.h>      // wld_<types>_t

  // APIs
#include "twc_image_file_helper.h"  // Our API to ensure consistancy

 // -------------------------
 // JPEG File Helper Routines

  // For JPEG images, we use jpeglib
#include <jpeglib.h>

  // Associate a libjpeg error manager with a jmp_buf
struct error_label {
    struct jpeg_error_mgr jpeg_error_mgr;
    jmp_buf               jmp_buf;

    bool file_open;
    bool decompress_created;
    bool decompress_started;
};

  // Based on container_of macro from the Linux Kernel
#define error_label_of(ptr) ({                                                               \
    const __typeof__( ((struct error_label *)0)->jpeg_error_mgr ) *__mptr = (ptr);           \
    (struct error_label *)( (char *)__mptr - offsetof(struct error_label,jpeg_error_mgr) );} \
)

static
void
decompress_error_handler(
    j_common_ptr cinfo
) {
    // The data types
    //   jpeg_compress_struct    and
    //   jpeg_decompress_struct
    // contain some member fields in common.
    //
    // The pointer type
    //    j_common_ptr
    // is a libjpeg kludge denoting a pointer to either data type.  It
    // should only be used to reference common members.
    //
    // The component
    //   struct jpeg_error_mgr *err
    // is one of the common members.
    //
    // Before the setjmp, we arrange that
    //   cinfo->err  points_to  error_label.jpeg_error_mgr

      // Recover the error label
    struct error_label *error_label = error_label_of( cinfo->err );

      // Unwind the stack back to the setjmp call
    longjmp(error_label->jmp_buf, 1);
}

extern
size_t
twc_jpeg_file_operation(
    enum jpeg_action j_action,
    const char      *jpeg_name,
    wld_dimension_t *width,
    wld_dimension_t *height,
    wld_dimension_t *stride,
    wld_pixel_t     *pixel_memory,
    const size_t     pix_mem_size
) {
    // stategy for using libjpeg:
    //
    //   1) open jpeg file and determine decompressed dimensions
    //
    //   2) if (operation == JA_dimensions) we're done, else continue
    //
    //   3) decompress the raw data, placing it into pixel_memory
    //
    //   4) make sure that the decomressed pixal format is
    //      WL_SHM_FORMAT_ARGB8888

      // libjpeg uses a data type to hold decompression info
    struct jpeg_decompress_struct cinfo;

      // Our error info
    struct error_label error_label = {
        .file_open          = false,
        .decompress_created = false,
        .decompress_started = false,
    };

      // This FILE is the source for decompression
    FILE *jpeg_file;

      // Initailize our libjeg error manager and record it in cinfo.
    cinfo.err = jpeg_std_error( &error_label.jpeg_error_mgr );

      // Replace the standard error handler with ours.  (The standard
      // can call exit().)
    error_label.jpeg_error_mgr.error_exit = decompress_error_handler;

    if ( setjmp(error_label.jmp_buf) != 0 ) {

        // An error occured.
        // The decompress_error_handler() longjmp'ed us back here.
        // Clean up.

        if ( error_label.decompress_started == true ) {
            jpeg_finish_decompress( &cinfo);
        }

        if ( error_label.decompress_created == true ) {
            jpeg_destroy_decompress(&cinfo);
        }

        if ( error_label.file_open == true ) {
            fclose(jpeg_file);
        }

        return 0;
    }

    jpeg_create_decompress( &cinfo );
    error_label.decompress_created = true;

      // The FILE named jpeg_name is the source for decompression
    if ((jpeg_file = fopen(jpeg_name, "rb")) == NULL) {
        jpeg_destroy_decompress(&cinfo);
        return 0;
     }
    error_label.file_open = true;

      // Tell libjpeg that the open file is the decompression source
    jpeg_stdio_src( &cinfo, jpeg_file );

      // "read start of JPEG datastream to see what's there"
      //
      // This call sets the out_color_space value to some default.
      // We'll change it below.
    if ( jpeg_read_header(&cinfo, TRUE) != JPEG_HEADER_OK ) {
        jpeg_destroy_decompress(&cinfo);
        fclose(jpeg_file);
        return 0;
    }

    // Many jpeg images are formated as RGBRGBRGB...  We want to
    // include an (opaque) Alpha channel for a total of 32 bits.

      // Set the pixel format for the decompressed image.  Note:
      // JCS_EXT_RGBA does not work - the resulting pixels have red
      // and blue reversed.  Perhaps an endian thing.
    cinfo.out_color_space  = JCS_EXT_BGRA;
    size_t bytes_per_pixel = 4;

      // Setup decompression using the conf values in cinfo.  This is
      // a prerequisite to determining image dimensions.
    jpeg_start_decompress( &cinfo );
    error_label.decompress_started = true;

      // Get select decompression attributes.
    *width  = cinfo.output_width;
    *height = cinfo.output_height;
    *stride = *width * bytes_per_pixel;

    size_t image_stride = *stride;
    size_t image_size   = *stride * *height;

    if ( j_action == JA_dimensions ) {
        // Early return

        jpeg_destroy_decompress(&cinfo);
        fclose(jpeg_file);

        return image_size;
    }

    if ( j_action     != JA_read_to_buffer ||
         pix_mem_size <  image_size
    ) {
        jpeg_destroy_decompress(&cinfo);
        fclose(jpeg_file);

        return 0;
    }

    // Decompress from the file to pixel_memory.
    //
    // A JSAMPLE is an unsigned char.
    // A sequence of JSAMPLEs is a row (aka scanline).
    // A JSAMPROW is a pointer to the first JSAMPLE in a row.
    // A JSAMPARRAY is an array of JSAMPROWs.

      // We'll decompress one scan row at-a-time.
    JDIMENSION max_lines = 1;
    JSAMPLE   *j_samp_array[max_lines];
    while ( cinfo.output_scanline < cinfo.output_height ) {
          // Let j_samp_array[0] point to the next dest row
        j_samp_array[0] = (
            (uint8_t*)pixel_memory +
           (cinfo.output_scanline) * image_stride
        );
        jpeg_read_scanlines( &cinfo, j_samp_array, max_lines);
    }

    jpeg_finish_decompress( &cinfo);
    jpeg_destroy_decompress(&cinfo);
    fclose(jpeg_file);

    return image_size;
}

 // ------------------------
 // PNG File Helper Routines

  // For PNG images, we use cairo
#include <cairo/cairo.h>

extern
size_t
twc_helper_png_dimensions(
    const char      *png_name,
    wld_dimension_t *width,
    wld_dimension_t *height,
    wld_dimension_t *stride
) {
    cairo_surface_t *surface = (
        cairo_image_surface_create_from_png(png_name)
    );

    if ( surface == NULL ) { return 0; }

    *width  = cairo_image_surface_get_width( surface);
    *height = cairo_image_surface_get_height(surface);
    *stride = cairo_image_surface_get_stride(surface);

    size_t image_size = *height * *stride;

    cairo_surface_destroy(surface);

    return image_size;
}

extern
size_t
twc_helper_png_to_buffer(
    const char  *png_name,
    wld_pixel_t *pixel_memory,
    const size_t pix_mem_size
) {
    cairo_surface_t *surface = (
        cairo_image_surface_create_from_png(png_name)
    );

    if ( surface == NULL ) { return 0; }

    wld_dimension_t height = cairo_image_surface_get_height(surface);
    wld_dimension_t stride = cairo_image_surface_get_stride(surface);

    size_t image_size = height * stride;

    if ( pix_mem_size < image_size ) { return 0; }

    unsigned char *png_src = (
        cairo_image_surface_get_data(surface)
    );

    if ( png_src == NULL ) {
        cairo_surface_destroy(surface);
        return 0;
    }

    memcpy(pixel_memory, (void*)png_src, image_size);

    cairo_surface_destroy(surface);

    return image_size;
}

 // ------------------------
 // SVG File Helper Routines
 //
 // Bug:
 //   https://gitlab.gnome.org/GNOME/librsvg/-/issues/1136
 // Fix:
 //   https://gitlab.gnome.org/federico/librsvg/-/commit/25001fc0c4e6f5a88d76eb876a48e3ec56c104fa


  // For SVG images, we use librsvg (and cairo)
//#include <librsvg/rsvg.h>     Currently buggy use alternative
#include "bugfix_rsvg.h"

// https://gnome.pages.gitlab.gnome.org/librsvg/Rsvg-2.0/class.Handle.html
// https://gnome.pages.gitlab.gnome.org/librsvg/Rsvg-2.0/overview.html
// https://gnome.pages.gitlab.gnome.org/librsvg/Rsvg-2.0/ctor.Handle.new_from_gfile_sync.html
// https://gnome.pages.gitlab.gnome.org/librsvg/Rsvg-2.0/method.Handle.render_document.html
// https://gnome.pages.gitlab.gnome.org/librsvg/Rsvg-2.0/method.Handle.set_dpi.html

// https://cairographics.org/manual/cairo-Image-Surfaces.html#cairo-image-surface-get-data
// https://cairographics.org/manual/cairo-cairo-t.html#cairo-create

// https://docs.gtk.org/gio/type_func.File.new_for_path.html

extern
size_t
twc_helper_svg_to_buffer(
    const char           *svg_name,
    const wld_dimension_t width,
    const wld_dimension_t height,
    wld_pixel_t          *pixel_memory,
    const size_t          pix_mem_size
) {
#   define MAX_WINDOW_WIDTH  (3840)
#   define MAX_WINDOW_HEIGHT (2160)

    if ( MAX_WINDOW_WIDTH  < width  ||
         MAX_WINDOW_HEIGHT < height
    ) { return 0; }

    bool file_open       = false;
    bool handle_created  = false;
    bool surface_created = false;
    bool context_created = false;

    size_t bytes_per_pixel = 4;
    wld_dimension_t stride = height * bytes_per_pixel;
    size_t      image_size = height * stride;
    size_t  ret_image_size = 0;

      // Open SVG file
    GError *error = NULL;
    GFile  *FILE  = g_file_new_for_path( svg_name );
    if ( FILE == NULL ) { goto cleanup; }
    file_open = true;

      // rsvg_handle
    RsvgHandle *rsvg_handle = (
        rsvg_handle_new_from_gfile_sync(
            FILE,
            RSVG_HANDLE_FLAGS_NONE,
            NULL,
            &error
        )
    );
    if ( rsvg_handle == NULL ) { goto cleanup; }
    handle_created = true;

      // cairo surface
    cairo_surface_t *cairo_surface = (
        cairo_image_surface_create(
            CAIRO_FORMAT_ARGB32,
            width,
            height
        )
    );
    cairo_status_t cairo_srf_status = (
        cairo_surface_status( cairo_surface )
    );
    if ( cairo_srf_status != CAIRO_STATUS_SUCCESS ) { goto cleanup; }
    surface_created = true;

      // cairo context
    cairo_t            *cairo_context = cairo_create( cairo_surface );
    cairo_status_t cairo_cntxt_status = (
        cairo_status( cairo_context )
    );
    if ( cairo_cntxt_status != CAIRO_STATUS_SUCCESS ) { goto cleanup; }
    context_created = true;

      // Typical monitor is 96 dpi
    rsvg_handle_set_dpi ( rsvg_handle, 96.0 );

    RsvgRectangle viewport = {
        .x      = 0.0,
        .y      = 0.0,
        .width  = width,
        .height = height,
    };

      // Render the SVG image at viewport scale
    gboolean render_OK = (
        rsvg_handle_render_document(
            rsvg_handle,
            cairo_context,
           &viewport,
           &error
         )
    );
    if ( ! render_OK ) { goto cleanup; }

      // Get a reference to the pixel buffer
    unsigned char *svg_src = (
        cairo_image_surface_get_data( cairo_surface )
    );
    if ( svg_src == NULL ) { goto cleanup; }

    memcpy( pixel_memory, (void*)svg_src, image_size );

    ret_image_size = image_size;

cleanup:
    if ( context_created ) { cairo_destroy(         cairo_context); }
    if ( surface_created ) { cairo_surface_destroy( cairo_surface); }
    if (  handle_created ) { g_object_unref( rsvg_handle ); }
    if ( file_open       ) { g_object_unref( FILE ); }

    return ret_image_size;
}

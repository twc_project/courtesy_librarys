#ifndef XX_DESKTOP_CUES_INERT_H
#define XX_DESKTOP_CUES_INERT_H

  // Protocols
#include <desktop-cues-v1-client-protocol.h>

 // --------------------
 // xx_desktop_cues_base

extern
void
inert_xx_desktop_cues_base_event_revoked(
    void  *data,
    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base
);

extern
const
struct xx_desktop_cues_base_v1_listener inert_xx_desktop_cues_base_v1_listener;
#define INERT_XX_DESKTOP_CUES_BASELISTENER (&inert_xx_desktop_cues_base_listener)

 // --------------
 // xx_desktop_cue

extern
void
inert_xx_desktop_cue_event_take(
    void  *data,
    struct xx_desktop_cue *xx_desktop_cue
);

extern
const
struct xx_desktop_cue_listener inert_xx_desktop_cue_listener;
#define INERT_XX_DESKTOP_CUE_LISTENER (&inert_xx_desktop_cue_listener)

#endif // XX_DESKTOP_CUES_INERT_H

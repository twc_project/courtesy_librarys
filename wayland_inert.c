
  // System
#include <stdint.h>     // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

  // Protocols
#include <wayland-client-protocol.h>    // struct wl_<interface>
                                        // struct wl_<interface>_listener

  // APIs
#include "wayland_inert.h"  // Our API to ensure consistancy

 // ----------
 // wl_display

extern
void
inert_wl_display_event_error(
    void              *data,
    struct wl_display *wl_display,
    void              *object_id,
    uint32_t           code,
    const char        *message
) { /* no-op */ return; }

extern
void
inert_wl_display_event_delete_id(
    void              *data,
    struct wl_display *wl_display,
    uint32_t           id
) { /* no-op */ return; }

const struct wl_display_listener inert_wl_display_listener = {
    .error     = inert_wl_display_event_error,
    .delete_id = inert_wl_display_event_delete_id,
};

 // -----------
 // wl_registry

extern
void
inert_wl_registry_event_global(
    void               *data,
    struct wl_registry *wl_registry,
    uint32_t            name,
    const char         *interface,
    uint32_t            version
) { /* no-op */ return; }

extern
void
inert_wl_registry_event_global_remove(
    void               *data,
    struct wl_registry *wl_registry,
    uint32_t            name
) { /* no-op */ return; }

const struct wl_registry_listener inert_wl_registry_listener = {
    .global        = inert_wl_registry_event_global,
    .global_remove = inert_wl_registry_event_global_remove,
};

 // ------
 // wl_shm

extern
void
inert_wl_shm_event_format(
    void          *data,
    struct wl_shm *wl_shm,
    uint32_t       format
) { /* no-op */ return; }

const struct wl_shm_listener inert_wl_shm_listener = {
    .format = inert_wl_shm_event_format,
};

 // ---------
 // wl_buffer

extern
void
inert_wl_buffer_event_release(
    void             *data,
    struct wl_buffer *wl_buffer
) { /* no-op */ return; }

const struct wl_buffer_listener inert_wl_buffer_listener = {
    .release = inert_wl_buffer_event_release,
};

 // ----------
 // wl_surface

extern
void
inert_wl_surface_event_enter(
    void              *data,
    struct wl_surface *wl_surface,
    struct wl_output  *output
) { /* no-op */ return; }

extern
void
inert_wl_surface_event_leave(
    void              *data,
    struct wl_surface *wl_surface,
    struct wl_output  *output
) { /* no-op */ return; }

const struct wl_surface_listener inert_wl_surface_listener = {
    .enter = inert_wl_surface_event_enter,
    .leave = inert_wl_surface_event_leave,
};

 // -------
 // wl_seat

extern
void
inert_wl_seat_event_capabilities(
    void           *data,
    struct wl_seat *wl_seat,
    uint32_t        capabilities
) { /* no-op */ return; }

extern
void
inert_wl_seat_event_name(
    void           *data,
    struct wl_seat *wl_seat,
    const char     *name
) { /* no-op */ return; }

const struct wl_seat_listener inert_wl_seat_listener = {
    .capabilities = inert_wl_seat_event_capabilities,
    .name         = inert_wl_seat_event_name,
};

 // ----------
 // wl_pointer

extern
void
inert_wl_pointer_event_enter(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *surface,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_leave(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *surface
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_motion(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_button(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    uint32_t           time,
    uint32_t           button,
    uint32_t           state
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_axis(
        void *data,
        struct wl_pointer *wl_pointer,
        uint32_t           time,
        uint32_t           axis,
        wl_fixed_t         value
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_frame(
    void *data,
    struct wl_pointer *wl_pointer
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_axis_source(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           axis_source
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_axis_stop(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           axis
) { /* no-op */ return; }

extern
void
inert_wl_pointer_event_axis_discrete(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           axis,
     int32_t           discrete
) { /* no-op */ return; }

const struct wl_pointer_listener inert_wl_pointer_listener = {
    .enter         = inert_wl_pointer_event_enter,
    .leave         = inert_wl_pointer_event_leave,
    .motion        = inert_wl_pointer_event_motion,
    .button        = inert_wl_pointer_event_button,
    .axis          = inert_wl_pointer_event_axis,
    .frame         = inert_wl_pointer_event_frame,
    .axis_source   = inert_wl_pointer_event_axis_source,
    .axis_stop     = inert_wl_pointer_event_axis_stop,
    .axis_discrete = inert_wl_pointer_event_axis_discrete,
};

 // -----------
 // wl_keyboard

extern
void
inert_wl_keyboard_event_keymap(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            format,
     int32_t            fd,
    uint32_t            size
) { /* no-op */ return; }

extern
void
inert_wl_keyboard_event_enter(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    struct wl_surface  *wl_surface,
    struct wl_array    *keys
) { /* no-op */ return; }

extern
void
inert_wl_keyboard_event_leave(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    struct wl_surface  *wl_surface
) { /* no-op */ return; }

extern
void
inert_wl_keyboard_event_key(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    uint32_t            time,
    uint32_t            key,
    uint32_t            state
) { /* no-op */ return; }

extern
void
inert_wl_keyboard_event_modifiers(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    uint32_t            mods_depressed,
    uint32_t            mods_latched,
    uint32_t            mods_locked,
    uint32_t            group
) { /* no-op */ return; }

extern
void
inert_wl_keyboard_event_repeat_info(
    void *data,
    struct wl_keyboard *wl_keyboard,
    int32_t             rate,
    int32_t             delay
) { /* no-op */ return; }

const struct wl_keyboard_listener inert_wl_keyboard_listener = {
    .keymap      = inert_wl_keyboard_event_keymap,
    .enter       = inert_wl_keyboard_event_enter,
    .leave       = inert_wl_keyboard_event_leave,
    .key         = inert_wl_keyboard_event_key,
    .modifiers   = inert_wl_keyboard_event_modifiers,
    .repeat_info = inert_wl_keyboard_event_repeat_info,
};

 // ---------
 // wl_output

extern
void
inert_wl_output_event_geometry(
    void *data,
    struct wl_output *wl_output,
    int32_t           x,
    int32_t           y,
    int32_t           physical_width,
    int32_t           physical_height,
    int32_t           subpixel,
    const char       *make,
    const char       *model,
    int32_t           transform
) { /* no-op */ return; }

extern
void
inert_wl_output_event_mode(
    void *data,
    struct wl_output *wl_output,
    uint32_t          flags,
     int32_t          width,
     int32_t          height,
     int32_t          refresh
) { /* no-op */ return; }

extern
void
inert_wl_output_event_done(
    void *data,
    struct wl_output *wl_output
) { /* no-op */ return; }

extern
void
inert_wl_output_event_scale(
    void *data,
    struct wl_output *wl_output,
    int32_t           factor
) { /* no-op */ return; }

extern
void
inert_wl_output_event_name(
    void *data,
    struct wl_output *wl_output,
    const  char      *name
) { /* no-op */ return; }

extern
void
inert_wl_output_event_description(
    void *data,
    struct wl_output *wl_output,
    const  char      *description
) { /* no-op */ return; }

const struct wl_output_listener inert_wl_output_listener = {
    .geometry    = inert_wl_output_event_geometry,
    .mode        = inert_wl_output_event_mode,
    .done        = inert_wl_output_event_done,
    .scale       = inert_wl_output_event_scale,
    .name        = inert_wl_output_event_name,
    .description = inert_wl_output_event_description,
};

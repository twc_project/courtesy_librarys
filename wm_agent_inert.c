
  // System
#include <stdint.h>     // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

  // Protocols
#include <wm-agent-v1-client-protocol.h>

  // APIs
#include "wm_agent_inert.h"    // Our API to ensure consistancy

 // ----------------
 // wm_agent_base_v1

#if 0
extern
void
inert_wm_agent_base_v1_event_interactive_agent_granted(
    void  *data,
    struct wm_agent_base_v1        *wm_agent_base_v1,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_menu_agent_granted(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1,
    struct xx_wm_menu_agent *xx_wm_menu_agent
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_decoration_stylist_granted(
    void  *data,
    struct wm_agent_base_v1         *wm_agent_base_v1,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_icon_stylist_granted(
    void  *data,
    struct wm_agent_base_v1   *wm_agent_base_v1,
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_workspace_agent_granted(
    void  *data,
    struct wm_agent_base_v1      *wm_agent_base_v1,
    struct xx_wm_workspace_agent *xx_wm_workspace_agent
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_task_agent_granted(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1,
    struct xx_wm_task_agent *xx_wm_task_agent
) { /* no-op */ return; }
#endif

extern
void
inert_wm_agent_base_v1_event_new_agent_action(
    void  *data,
    struct wm_agent_base_v1       *wm_agent_base_v1,
    enum wm_agent_base_v1_agent_id agent_id,
    const char                    *name,
    uint32_t                       action_id
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_new_agent(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1,
    uint32_t                 name,
    const char              *interface,
    uint32_t                 version
) { /* no-op */ return; }

extern
void
inert_wm_agent_base_v1_event_restart(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1
) { /* no-op */ return; }

const
struct wm_agent_base_v1_listener inert_wm_agent_base_v1_listener = {
#if 0
    .interactive_agent_granted  = inert_wm_agent_base_v1_event_interactive_agent_granted,
    .menu_agent_granted         = inert_wm_agent_base_v1_event_menu_agent_granted,
    .decoration_stylist_granted = inert_wm_agent_base_v1_event_decoration_stylist_granted,
    .icon_stylist_granted       = inert_wm_agent_base_v1_event_icon_stylist_granted,
    .workspace_agent_granted    = inert_wm_agent_base_v1_event_workspace_agent_granted,
    .task_agent_granted         = inert_wm_agent_base_v1_event_task_agent_granted,
#endif
    .new_agent_action           = inert_wm_agent_base_v1_event_new_agent_action,
    .new_agent                  = inert_wm_agent_base_v1_event_new_agent,
    .restart                    = inert_wm_agent_base_v1_event_restart,
};

 // -----------------------
 // xx_wm_interactive_agent

extern
void
inert_xx_wm_interactive_agent_event_dismiss(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_interactive_agent_event_annex_revoked(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_interactive_agent_event_select(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_interactive_agent_event_track(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_interactive_agent_event_stop(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
) { /* no-op */ return; }

const
struct xx_wm_interactive_agent_listener inert_xx_wm_interactive_agent_listener = {
    .dismiss       = inert_xx_wm_interactive_agent_event_dismiss,
    .annex_revoked = inert_xx_wm_interactive_agent_event_annex_revoked,
    .select        = inert_xx_wm_interactive_agent_event_select,
    .track         = inert_xx_wm_interactive_agent_event_track,
    .stop          = inert_xx_wm_interactive_agent_event_stop,
};

 // ----------------------
 // xx_wm_decoration_stylist

extern
void
inert_xx_wm_decoration_stylist_event_dismiss(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }

#if 0
extern
void
inert_xx_wm_decoration_stylist_event_deco_style(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }

extern
void
inert_xx_wm_decoration_stylist_event_titlebar_attachment(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }

extern
void
inert_xx_wm_decoration_stylist_event_frame_thickness(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }

extern
void
inert_xx_wm_decoration_stylist_event_titlebar_thickness(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }

extern
void
inert_xx_wm_decoration_stylist_event_highlight_style(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
) { /* no-op */ return; }
#endif

const
struct xx_wm_decoration_stylist_listener inert_xx_wm_decoration_stylist_listener = {
    .dismiss = inert_xx_wm_decoration_stylist_event_dismiss,
};

 // ----------------
 // xx_wm_decoration
 //
 //   xx_wm_decoration has no events

#if 0
extern
void
inert_xx_wm_decoration_event_dimension(
    void  *data,
    struct xx_wm_decoration *xx_wm_decoration,
    uint32_t                 width,
    uint32_t                 height
) { /* no-op */ return; }

const
struct xx_wm_decoration_listener inert_xx_wm_decoration_listener = {
    .dimension = inert_xx_wm_decoration_event_dimension,
};
#endif

 // ------------------
 // xx_wm_icon_stylist

extern
void
inert_xx_wm_icon_stylist_event_dismiss(
    void  *data,
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
) { /* no-op */ return; }

const
struct xx_wm_icon_stylist_listener inert_xx_wm_icon_stylist_listener = {
    .dismiss = inert_xx_wm_icon_stylist_event_dismiss,
};

 // ----------
 // xx_wm_icon

extern
void
inert_xx_wm_icon_event_xdg_toplevel_icon(
    void  *data,
    struct xx_wm_icon *xx_wm_icon,
    const  char       *icon_name
) { /* no-op */ return; }

extern
void
inert_xx_wm_icon_event_xdg_toplevel_icon_buffer(
    void  *data,
    struct xx_wm_icon *xx_wm_icon,
    int32_t            size,
    int32_t            scale
) { /* no-op */ return; }

const
struct xx_wm_icon_listener inert_xx_wm_icon_listener = {
    .xdg_toplevel_icon        = inert_xx_wm_icon_event_xdg_toplevel_icon,
    .xdg_toplevel_icon_buffer = inert_xx_wm_icon_event_xdg_toplevel_icon_buffer,
};

 // --------------
 // xx_wm_menu_agent

extern
void
inert_xx_wm_menu_agent_event_dismiss(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_menu_agent_event_annex_revoked(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_menu_agent_event_keyboard_shortcut(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 shortcut_key,
    uint32_t                 shortcut_key_state,
    uint32_t                 mods_depressed,
    uint32_t                 shortcut_mod_state
) { /* no-op */ return; }

extern
void
inert_xx_wm_menu_agent_event_new_service(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 numeric_name,
    uint32_t                 version,
    const  char             *provider_name,
    const  char             *service_name
) { /* no-op */ return; }

const
struct xx_wm_menu_agent_listener inert_xx_wm_menu_agent_listener = {
    .dismiss           = inert_xx_wm_menu_agent_event_dismiss,
    .annex_revoked     = inert_xx_wm_menu_agent_event_annex_revoked,
    .keyboard_shortcut = inert_xx_wm_menu_agent_event_keyboard_shortcut,
    .new_service       = inert_xx_wm_menu_agent_event_new_service,
};

 // -------------------
 // xx_wm_workspace_agent

extern
void
inert_xx_wm_workspace_agent_event_dismiss(
    void  *data,
    struct xx_wm_workspace_agent *xx_wm_workspace_agent
) { /* no-op */ return; }

extern
void
inert_xx_wm_workspace_agent_event_configure_output_mode(
    void  *data,
    struct xx_wm_workspace_agent *xx_wm_workspace_agent,
    uint32_t                      mode
) { /* no-op */ return; }

const
struct xx_wm_workspace_agent_listener inert_xx_wm_workspace_agent_listener = {
    .dismiss               = inert_xx_wm_workspace_agent_event_dismiss,
    .configure_output_mode = inert_xx_wm_workspace_agent_event_configure_output_mode,
};

 // ----------------
 // xx_wm_task_agent

extern
void
inert_xx_wm_task_agent_event_dismiss(
    void  *data,
    struct xx_wm_task_agent *xx_wm_task_agent
) { /* no-op */ return; }

const
struct xx_wm_task_agent_listener inert_xx_wm_task_agent_listener = {
    .dismiss = inert_xx_wm_task_agent_event_dismiss,
};

 // -------------------
 // xx_wm_window_registry

extern
void
inert_xx_wm_window_registry_event_new(
    void  *data,
    struct xx_wm_window_registry *xx_wm_window_registry,
    uint32_t                   name,
    uint32_t                   version
) { /* no-op */ return; }

const
struct xx_wm_window_registry_listener inert_xx_wm_window_registry_listener = {
    .new  = inert_xx_wm_window_registry_event_new,
};

 // ----------
 // xx_wm_window

extern
void
inert_xx_wm_window_event_iconify_state(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t            state
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_display_state(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t            state
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_app_id(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    const  char         *app_id
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_title(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    const  char         *title
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_location(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    wl_fixed_t           x,
    wl_fixed_t           y
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_icon_location(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    wl_fixed_t           x,
    wl_fixed_t           y
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_dimension(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_icon_dimension(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_occupancy(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             occupancy
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_enter_output(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    struct wl_output    *output
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_pointer_focus(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             focus
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_keyboard_focus(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             focus
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_selection_focus(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             focus
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_thumbnail_max_size(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_action(
    void  *data,
    struct xx_wm_window           *xx_wm_window,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
) { /* no-op */ return; }

extern
void
inert_xx_wm_window_event_expired(
    void  *data,
    struct xx_wm_window *xx_wm_window
) { /* no-op */ return; }

const
struct xx_wm_window_listener inert_xx_wm_window_listener = {
    .iconify_state      = inert_xx_wm_window_event_iconify_state,
    .display_state      = inert_xx_wm_window_event_display_state,
    .app_id             = inert_xx_wm_window_event_app_id,
    .title              = inert_xx_wm_window_event_title,
    .location           = inert_xx_wm_window_event_location,
    .icon_location      = inert_xx_wm_window_event_location,
    .dimension          = inert_xx_wm_window_event_dimension,
    .icon_dimension     = inert_xx_wm_window_event_icon_dimension,
    .occupancy          = inert_xx_wm_window_event_occupancy,
    .enter_output       = inert_xx_wm_window_event_enter_output,
    .pointer_focus      = inert_xx_wm_window_event_pointer_focus,
    .keyboard_focus     = inert_xx_wm_window_event_keyboard_focus,
    .selection_focus    = inert_xx_wm_window_event_selection_focus,
    .thumbnail_max_size = inert_xx_wm_window_event_thumbnail_max_size,
    .action             = inert_xx_wm_window_event_action,
    .expired            = inert_xx_wm_window_event_expired,
};

 // ------------------------
 // xx_wm_workspace_registry

extern
void
inert_xx_wm_workspace_registry_event_new(
    void  *data,
    struct xx_wm_workspace_registry *xx_wm_workspace_registry,
    uint32_t                         name,
    uint32_t                         version
) { /* no-op */ return; }

const
struct xx_wm_workspace_registry_listener inert_xx_wm_workspace_registry_listener = {
    .new = inert_xx_wm_workspace_registry_event_new,
};

 // ---------------
 // xx_wm_workspace

extern
void
inert_xx_wm_workspace_event_active(
    void  *data,
    struct xx_wm_workspace *xx_wm_workspace,
    struct wl_output       *output
) { /* no-op */ return; }

extern
void
inert_xx_wm_workspace_event_name(
    void  *data,
    struct xx_wm_workspace *xx_wm_workspace,
    const char             *output
) { /* no-op */ return; }

extern
void
inert_xx_wm_workspace_event_expired(
    void  *data,
    struct xx_wm_workspace *xx_wm_workspace
) { /* no-op */ return; }

const
struct xx_wm_workspace_listener inert_xx_wm_workspace_listener = {
    .active  = inert_xx_wm_workspace_event_active,
    .name    = inert_xx_wm_workspace_event_name,
    .expired = inert_xx_wm_workspace_event_expired,
};

 // ------------------------
 // xx_desktop_cue_controller

extern
void
inert_xx_desktop_cue_controller_event_abandon(
    void  *data,
    struct xx_desktop_cue_controller *xx_desktop_cue_controller
) { /* no-op */ return; }

const
struct xx_desktop_cue_controller_listener inert_xx_desktop_cue_controller_listener = {
    .abandon = inert_xx_desktop_cue_controller_event_abandon,
};

 // -------------------------
 // xx_zwm_service_controller

extern
void
inert_xx_zwm_service_controller_event_new_service_operation(
    void  *data,
    struct xx_zwm_service_controller *xx_zwm_service_controller,
    const  char                      *name,
    uint32_t                          operation
) { /* no-op */ return; }

extern
void
inert_xx_zwm_service_controller_event_abandon(
    void  *data,
    struct xx_zwm_service_controller *xx_zwm_service_controller
) { /* no-op */ return; }

const
struct xx_zwm_service_controller_listener inert_xx_zwm_service_controller_listener = {
    .new_service_operation = inert_xx_zwm_service_controller_event_new_service_operation,
    .abandon               = inert_xx_zwm_service_controller_event_abandon,
};


  // Protocols
#include <desktop-cues-v1-client-protocol.h>

  // APIs
#include "desktop_cues_inert.h"     // Our API to ensure consistancy


 // --------------------
 // xx_desktop_cues_base

extern
void
inert_xx_desktop_cues_base_event_revoked(
    void  *data,
    struct xx_desktop_cues_base_v1 *xx_desktop_cues_base
) { /* no-op */ return; }

const
struct xx_desktop_cues_base_v1_listener inert_xx_desktop_cues_base_v1_listener = {
    .revoked = inert_xx_desktop_cues_base_event_revoked,
};

 // --------------
 // xx_desktop_cue

extern
void
inert_xx_desktop_cue_event_take(
    void  *data,
    struct xx_desktop_cue *xx_desktop_cue
) { /* no-op */ return; }

const
struct xx_desktop_cue_listener inert_xx_desktop_cue_listener = {
    .take = inert_xx_desktop_cue_event_take,
};

#ifndef TWC_LOG_H
#define TWC_LOG_H

 /*
 ** Use the log system from the wcf (wlroots)
 */

  //https://github.com/swaywm/wlroots/blob/master/include/wlr/util/log.h
  // The __FILE__ name in the log message starts with the path from
  // the meson build dir to the source dir.  This macro chops that
  // prefix path in the log output.  (It actually chops n chars from
  // the front of __FILE__, where n is the length of this string.
#define WLR_REL_SRC_DIR "../"
#include <wlr/util/log.h>

#define LOG_INIT      wlr_log_init(WLR_DEBUG, NULL)
#define LOG_INIT_INFO wlr_log_init(WLR_INFO,  NULL)

#define LOG_ERROR(fmt, ...) \
    _wlr_log(WLR_ERROR, "[%s:%d] " fmt, _WLR_FILENAME, __LINE__, ##__VA_ARGS__)

#define LOG_INFO(fmt, ...) \
    _wlr_log(WLR_INFO, "[%s:%d] " fmt, _WLR_FILENAME, __LINE__, ##__VA_ARGS__)

  // Fix: add some levels
#define xLOG_DEBUG(fmt, ...) \
    _wlr_log(WLR_INFO, "[%s:%d] " fmt, _WLR_FILENAME, __LINE__, ##__VA_ARGS__)

#define LOG_DEBUG(fmt, ...)
#define LOG_TRACE(fmt, ...)

#endif  // TWC_LOG_H

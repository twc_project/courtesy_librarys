
  // Protocols
#include <wm-service-v1-client-protocol.h>

  // APIs
#include "wm_service_inert.h"   // Our API to ensure consistancy


 // -------------------
 // xx_zwm_service_base
 //
 //   There are no events

 // --------------
 // xx_zwm_service

extern
void
inert_xx_zwm_service_event_operation(
    void  *data,
    struct xx_zwm_service *xx_zwm_service,
    uint32_t               operation
) { /* no-op */ return; }

const
struct xx_zwm_service_listener inert_xx_zwm_service_listener = {
    .operation = inert_xx_zwm_service_event_operation,
};

 // -------------------
 // xx_zwm_service_window
 //
 //   There are no events

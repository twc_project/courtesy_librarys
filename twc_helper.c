
  // System
#include <stddef.h>     // size_t, NULL
#include <stdbool.h>
#include <string.h>     // strncmp
#include <stdint.h>     // uint32_t, ...

  // Wayland
#include <wayland-client-core.h>    // wl_display_connect, wl_display_roundtrip
                                    // wl_display_dispatch

  // Protocols
#include <wayland-client-protocol.h>                    // <protocol_interface>_interface
#include <xdg-shell-client-protocol.h>                  //      |
#include <xdg-decoration-unstable-v1-client-protocol.h> //      |
#include <wm-agent-v1-client-protocol.h>                //      |
#include <wm-service-v1-client-protocol.h>              //      |
#include <desktop-cues-v1-client-protocol.h>            //      v

  // APIs
#include "twc_helper.h"     // Our API to ensure consistancy
#include "wl_DeVault.h"     // POSIX shared memory (shm) wrappers
#include "twl_widget.h"     // twl_color_t
#include "wayland_inert.h"
#include "cursorfont.h"
#include "twc_log.h"

 // --------------------------
 // Server Connection Routines

static
void
wl_registry_event_global(
    void               *data,       // pointer to the client's wl_singletons struct
    struct wl_registry *wl_registry,
    uint32_t            name,       // numeric name of interface
    const char         *interface,  // string  name of interface
    uint32_t            version
) {
    // The wl_registry emits the event named 'global' to announce
    // interfaces.  This listener routine binds selected interfaces.
    // For wl_seat and wl_output, a client callback is also invoked.

    struct wl_singletons *wl_singletons = data;

    if ( strncmp(interface, wl_compositor_interface.name, MAX_INTERFACE_NAME_LEN) == 0 ) {
          // wl_compositor
        wl_singletons->wl_compositor = (
            wl_registry_bind(
                wl_registry,
                name,
                &wl_compositor_interface,
                version
            )
        );
    } else if ( strncmp(interface, wl_subcompositor_interface.name, MAX_INTERFACE_NAME_LEN) == 0 ) {
          // wl_subcompositor
        wl_singletons->wl_subcompositor = (
            wl_registry_bind(
                wl_registry,
                name,
                &wl_subcompositor_interface,
                version
            )
        );
    } else if ( strncmp(interface, wl_shm_interface.name, MAX_INTERFACE_NAME_LEN) == 0 ) {
          // wl_shm
        wl_singletons->wl_shm = (
            wl_registry_bind(
                wl_registry,
                name,
                &wl_shm_interface,
                version
            )
        );
    } else if ( strncmp(interface, xdg_wm_base_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // xdg_wm_base
        wl_singletons->xdg_wm_base = (
            wl_registry_bind(
                wl_registry,
                name,
                &xdg_wm_base_interface,
                version
            )
        );
    } else if ( strncmp(interface, zxdg_decoration_manager_v1_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // xdg_decoration_manager
        wl_singletons->xdg_decoration_manager = (
            wl_registry_bind(
                wl_registry,
                name,
                &zxdg_decoration_manager_v1_interface,
                version
            )
        );
    } else if ( strncmp(interface, wm_agent_base_v1_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // wm_agent_base_v1
        wl_singletons->wm_agent_base_v1 = (
            wl_registry_bind(
                wl_registry,
                name,
                &wm_agent_base_v1_interface,
                version
            )
        );

    } else if ( strncmp(interface, xx_zwm_service_base_v1_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // xx_zwm_service_base_v1
        wl_singletons->xx_zwm_service_base_v1 = (
            wl_registry_bind(
                wl_registry,
                name,
                &xx_zwm_service_base_v1_interface,
                version
            )
        );

    } else if ( strncmp(interface, xx_desktop_cues_base_v1_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // xx_desktop_cues_base_v1
        wl_singletons->xx_desktop_cues_base_v1 = (
            wl_registry_bind(
                wl_registry,
                name,
                &xx_desktop_cues_base_v1_interface,
                version
            )
        );

    } else if ( strncmp(interface, wl_seat_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // wl_seat
        struct wl_seat *wl_seat = (
            wl_registry_bind(
                wl_registry,
                name,
                &wl_seat_interface,
                version
            )
        );
        wl_singletons->new_wl_seat_routine( wl_seat );

    } else if ( strncmp(interface, wl_output_interface.name, MAX_INTERFACE_NAME_LEN ) == 0 ) {
          // wl_output
        struct wl_output *wl_output = (
            wl_registry_bind(
                wl_registry,
                name,
                &wl_output_interface,
                version
            )
        );
        wl_singletons->new_wl_output_routine ( wl_output );
    }

    return;
}

static
void
wl_registry_event_global_remove(
    void               *data,       // pointer to the client's wl_singletons struct
    struct wl_registry *wl_registry,
    uint32_t            name        // numeric name of interface
) {
    // Fix: Call a client supplied routine.  For anything other than a
    // wl_seat or wl_output, the client is likely to completely
    // shutdown.

    return;
}

static
struct wl_registry_listener wl_registry_listener = {
    .global        =       wl_registry_event_global,
    .global_remove = wl_registry_event_global_remove,
};

extern
bool
twc_helper_connect(
    char                 *name,
    struct wl_singletons *wl_singletons
) {
    wl_singletons->wl_display              = NULL;
    wl_singletons->wl_registry             = NULL;
    wl_singletons->wl_compositor           = NULL;
    wl_singletons->wl_subcompositor        = NULL;
    wl_singletons->wl_shm                  = NULL;
    wl_singletons->xdg_wm_base             = NULL;
    wl_singletons->xdg_decoration_manager  = NULL;
    wl_singletons->wm_agent_base_v1        = NULL;
    wl_singletons->xx_desktop_cues_base_v1 = NULL;

    if ( wl_singletons->new_wl_seat_routine == NULL ) {
         wl_singletons->new_wl_seat_routine =  twc_helper_seat_not_used;
    }
    if ( wl_singletons->new_wl_output_routine == NULL ) {
         wl_singletons->new_wl_output_routine =  twc_helper_output_not_used;
    }

    struct wl_display *wl_display;

    wl_display = wl_display_connect(name);
    if ( wl_display == NULL ) {
        return false;
    }

    wl_singletons->wl_display = wl_display;

      // Create a client-specific registry.
    struct wl_registry *wl_registry = (
        wl_display_get_registry(wl_display)
    );
    wl_singletons->wl_registry = wl_registry;

      // Add wl_registry::global listener routines with the
      // client-specific wl_singletons struct as the user data.
    wl_registry_add_listener(wl_registry, &wl_registry_listener, wl_singletons);

      // This roundtrip will not return until the server has announced
      // all of its global singeltons thru the registry listener.
    wl_display_roundtrip(wl_display);

    return true;
}

 // ----------------------
 // Cursor Helper Routines

extern
int
twc_helper_lookup_Xcursor_by_name(
    char *cursor_name
) {
    if ( strncmp( cursor_name, "dot",      4 ) == 0 ) { return XC_dot;      }
    if ( strncmp( cursor_name, "fleur",    6 ) == 0 ) { return XC_fleur;    }
    if ( strncmp( cursor_name, "left_ptr", 9 ) == 0 ) { return XC_left_ptr; }
    if ( strncmp( cursor_name, "sizing",   7 ) == 0 ) { return XC_sizing;   }

    return XC_X_cursor;
}

extern
struct twc_cursor
twc_helper_cursor_create(
    struct wl_singletons *wl_singletons,
    int                   cursor_index,
    twl_color_t           foreground,
    twl_color_t           background
) {
    struct twc_cursor twc_cursor = {0};

    struct twc_shm_helper_pool *twc_shm_helper_pool = (
        &twc_cursor.twc_shm_helper_pool
    );

    size_t cursor_shm_size = (
        CURSOR_GLYPH_WIDTH     *
        CURSOR_GLYPH_HEIGHT    *
        CURSOR_BYTES_PER_PIXEL
    );

    twc_shm_helper_pool_create(
        wl_singletons->wl_shm,
        cursor_shm_size,
        twc_shm_helper_pool
    );

    void *shm_pool_base = (
        TWC_SHM_HELPER_GET_POOL_BASE(
            twc_shm_helper_pool
        )
    );

    struct wl_shm_pool *wl_shm_pool = (
        TWC_SHM_HELPER_GET_SHM_POOL(
            twc_shm_helper_pool
        )
    );

      // Create the cursor buffer
    struct wl_buffer *wl_buffer = (
        wl_shm_pool_create_buffer(
            wl_shm_pool,
            0,
            CURSOR_GLYPH_WIDTH,
            CURSOR_GLYPH_HEIGHT,
            CURSOR_GLYPH_WIDTH * CURSOR_BYTES_PER_PIXEL,  // stride = width*bytes/pixel
            WL_SHM_FORMAT_ARGB8888
        )
    );
    wl_buffer_add_listener(
        wl_buffer,
        INERT_WL_BUFFER_LISTENER,
        NULL
    );

      // Copy cursor image into the cursor buffer
    XC_hotspot_t hotspot = (
        twl_widget_render_X_cursor(
           shm_pool_base,
           cursor_index,
           BLACK,
           WHITE
        )
    );

      // Create the cursor surface
    struct wl_surface *wl_surface = (
        wl_compositor_create_surface(
            wl_singletons->wl_compositor
        )
    );
    wl_surface_add_listener(
        wl_surface,
        INERT_WL_SURFACE_LISTENER,
        NULL
    );

    wl_surface_attach(wl_surface, wl_buffer, 0,0);
    wl_surface_commit(wl_surface);

    twc_cursor.cursor_buffer   = wl_buffer;
    twc_cursor.cursor_surface  = wl_surface;
    twc_cursor.hotspot_x       = XC_HOTSPOT_x(hotspot);
    twc_cursor.hotspot_y       = XC_HOTSPOT_y(hotspot);

      // On return, copy the whole twc_cursor struct.  Caller must
      // allocate space.
    return twc_cursor;
}

extern
void
twc_helper_cursor_destroy(
    struct twc_cursor *twc_cursor
) {
    struct twc_shm_helper_pool *twc_shm_helper_pool = (
        &twc_cursor->twc_shm_helper_pool
    );

    wl_surface_attach(twc_cursor->cursor_surface, NULL, 0,0);
    wl_surface_commit(twc_cursor->cursor_surface);

    wl_surface_destroy (twc_cursor->cursor_surface );
    wl_buffer_destroy  (twc_cursor->cursor_buffer  );

    twc_shm_helper_pool_unmap( twc_shm_helper_pool );

    wl_shm_pool_destroy(
        TWC_SHM_HELPER_GET_SHM_POOL( twc_shm_helper_pool )
    );

    return;
}

extern
void
twc_helper_cursor_set(
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct twc_cursor *twc_cursor
) {
    wl_pointer_set_cursor(
        wl_pointer,
        serial,
        twc_cursor->cursor_surface,
        twc_cursor->hotspot_x,
        twc_cursor->hotspot_y
    );

    return;
}

 // -----------------------
 // Surface Helper Routines

extern
struct wl_surface*
twc_helper_surface_create(
    struct twc_helper_surface_basics *surface_basics,
    int32_t                           max_surface_size,
    struct wl_singletons             *wl_singletons,
    const struct wl_surface_listener *wl_surface_listener,
    void                             *data
) {
    /*
    ** The wl_shm_pool will be large enough to hold two buffers of
    ** max_surface_size.  If any buffer requires more pixels, the
    ** wl_surface must be destroyed and re-created.
    */

    if ( surface_basics == NULL ) { return false;; }

    surface_basics->wl_display           = wl_singletons->wl_display;
    surface_basics->max_surface_size     = max_surface_size;
    surface_basics->stated_buffer_width  = 0;
    surface_basics->stated_buffer_height = 0;

    int32_t shm_size = max_surface_size * 2;  // 2 max size buffers

    struct twc_shm_helper_pool *twc_shm_helper_pool = (
        &surface_basics->twc_shm_helper_pool
    );

    twc_shm_helper_pool_create(
        wl_singletons->wl_shm,
        shm_size,
        twc_shm_helper_pool
    );

    struct wl_shm_pool *wl_shm_pool = TWC_SHM_HELPER_GET_SHM_POOL(twc_shm_helper_pool);

      // Offsets are permanent for the life of a surface_basics struct
    surface_basics->pixel_buffer_offset[0] =            0;
    surface_basics->pixel_buffer_offset[1] = (int32_t)( max_surface_size );

    for (int i = 0; i < 2; i++) {

          // Start off with single-pixel (ie non-null) buffers to
          // avoid having to check for NULL pointers.
        surface_basics->wl_buffer[i] = (
            wl_shm_pool_create_buffer(
                wl_shm_pool,
                surface_basics->pixel_buffer_offset[i],
                1,
                1,
                1 * BYTES_PER_PIXEL,
                WL_SHM_FORMAT_XRGB8888
            )
        );
        surface_basics->free[i] = true;
    }

    surface_basics->next_buffer_index = 0;

      // Create the surface.
    surface_basics->wl_surface = (
        wl_compositor_create_surface(
            wl_singletons->wl_compositor
        )
    );

    wl_surface_add_listener(
        surface_basics->wl_surface,
        wl_surface_listener,
        data
    );

    return surface_basics->wl_surface;
}

extern
void
twc_helper_surface_destroy(
    struct twc_helper_surface_basics *surface_basics
) {
    if ( ! surface_basics->free[ 0 ] ||
         ! surface_basics->free[ 1 ]
    ) {
          // Clear wl_buffer from wl_surface
        wl_surface_attach( surface_basics->wl_surface , NULL, 0,0 );
        wl_surface_commit( surface_basics->wl_surface );

        wl_display_roundtrip( surface_basics->wl_display );
    }

    for (int i = 0; i < 2; i++) {
        wl_buffer_destroy( surface_basics->wl_buffer[i] );
    }

    struct twc_shm_helper_pool *twc_shm_helper_pool = (
        &surface_basics->twc_shm_helper_pool
    );

    struct wl_shm_pool *wl_shm_pool = (
        TWC_SHM_HELPER_GET_SHM_POOL( twc_shm_helper_pool )
    );

    if ( wl_shm_pool != NULL ) {

        twc_shm_helper_pool_unmap( twc_shm_helper_pool );

        wl_shm_pool_destroy( wl_shm_pool );
    }

    wl_surface_destroy( surface_basics->wl_surface );
}

  // Forward declaration
static
void
twc_helper_surface_check_buffers(
    struct twc_helper_surface_basics *surface_basics
);

static
void
twc_helper_buffer_event_release_0(
    void *data,
    struct wl_buffer *wl_buffer
) {
    struct twc_helper_surface_basics *surface_basics = data;

    surface_basics->free[0] = true;

    twc_helper_surface_check_buffers( surface_basics );

    return;
}

static
const
struct wl_buffer_listener twc_helper_buffer_listener_0 = {
    .release = twc_helper_buffer_event_release_0,
};

static
void
twc_helper_buffer_event_release_1(
    void *data,
    struct wl_buffer *wl_buffer
) {
    struct twc_helper_surface_basics *surface_basics = data;

    surface_basics->free[1] = true;

    twc_helper_surface_check_buffers( surface_basics );

    return;
}

static
const
struct wl_buffer_listener twc_helper_buffer_listener_1 = {
    .release = twc_helper_buffer_event_release_1,
};

static
void
twc_helper_surface_check_buffers(
    struct twc_helper_surface_basics *surface_basics
) {
    // Since a buffer which is in use cannot be updated, this routine
    // should be called whenever a buffer is released.

    int32_t stated_buffer_width  = surface_basics->stated_buffer_width;
    int32_t stated_buffer_height = surface_basics->stated_buffer_height;

    struct twc_shm_helper_pool *twc_shm_helper_pool = (
        &surface_basics->twc_shm_helper_pool
    );
    struct wl_shm_pool *wl_shm_pool = (
        TWC_SHM_HELPER_GET_SHM_POOL(twc_shm_helper_pool)
    );

    for (int i = 0; i < 2; i++) {

        if ( ! surface_basics->free[i] ) { continue; }

        if ( (surface_basics->buffer_width [i] == stated_buffer_width ) &&
             (surface_basics->buffer_height[i] == stated_buffer_height)
        ) { continue; }

        wl_buffer_destroy( surface_basics->wl_buffer[i] );

        surface_basics->wl_buffer[i] = (
            wl_shm_pool_create_buffer(
                wl_shm_pool,
                surface_basics->pixel_buffer_offset[i],
                stated_buffer_width,
                stated_buffer_height,
                stated_buffer_width * BYTES_PER_PIXEL,
                WL_SHM_FORMAT_XRGB8888
            )
        );

        surface_basics->buffer_width [i] = stated_buffer_width;
        surface_basics->buffer_height[i] = stated_buffer_height;

        wl_buffer_add_listener(
            surface_basics->wl_buffer[i],
            ( i ? &twc_helper_buffer_listener_1 : &twc_helper_buffer_listener_0 ),
            surface_basics
        );
    }

    return;
}

extern
bool
twc_helper_surface_update_buffers(
    struct twc_helper_surface_basics *surface_basics,
    int32_t                           new_buffer_width,
    int32_t                           new_buffer_height
) {
    int32_t new_buffer_size = (
        new_buffer_width  *
        new_buffer_height
    );

    if ( (surface_basics->max_surface_size <  new_buffer_size) ||
         (new_buffer_size                  <= 0              )
    )  { return false; }

    if ( (surface_basics->stated_buffer_width  == new_buffer_width ) &&
         (surface_basics->stated_buffer_height == new_buffer_height)
    ) { return true;  }

    surface_basics->stated_buffer_width  = new_buffer_width;
    surface_basics->stated_buffer_height = new_buffer_height;

    twc_helper_surface_check_buffers( surface_basics );

    return true;
}

extern
bool
twc_helper_surface_attach_commit(
    struct twc_helper_surface_basics *surface_basics
) {
    int next = surface_basics->next_buffer_index;

    if ( ! surface_basics->free[next] ) {
        return false;
    }

      // Advance the next buffer
    surface_basics->free[next]        = false;
    surface_basics->next_buffer_index = next ? 0 : 1;

    struct wl_surface *wl_surface  = surface_basics->wl_surface;
    struct wl_buffer  *next_buffer = surface_basics->wl_buffer[next];

    wl_surface_attach(wl_surface , next_buffer, 0,0);
    wl_surface_commit(wl_surface);

    return true;
}

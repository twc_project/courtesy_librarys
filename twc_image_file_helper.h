#ifndef TWC_IMAGE_FILE_HELPER_H
#define TWC_IMAGE_FILE_HELPER_H

  // TWC
#include <twc/wayland_types.h>      // wld_<types>_t

 // -------------------------
 // JPEG File Helper Routines

 /*
 ** libjpeg provides many tools for accessing a jpeg file including
 **
 **   1) determine image dimensions (eg width, height, stride)
 **   2) decompress the raw image into a memory buffer
 **
 ** The steps for Item 2 require all the steps for Item 1.  Therefore,
 ** a one-shot function to decompress an image into a buffer requires
 ** that the buffer be pre-allocated.  That's not possible without
 ** already knowing the dimensions of the image.
 **
 ** The image file helper routines provide two entry points:
 **
 **   twc_helper_jpeg_dimensions()      // get image dimensions
 **   twc_helper_jpeg_to_buffer()       // decompress image into a buffer
 **
 ** Buffer allocation may occur in-between.
 **
 ** These two routines are wrappers for a single function which does
 ** both Item 1 and 2.  In the get-image-dimensions case, the wrapper
 ** function returns early.  In the wrapper, the file is opened,
 ** (partially) read, and closed in both cases.  It is expected that
 ** these jpeg helper routines will be used sparingly, so the double
 ** overhead is negligible.
 */

enum jpeg_action{
    JA_nop,
    JA_dimensions,
    JA_read_to_buffer,
};

extern
size_t
twc_jpeg_file_operation(
    enum jpeg_action j_action,
    const char      *jpeg_name,
    wld_dimension_t *width,
    wld_dimension_t *height,
    wld_dimension_t *stride,
    wld_pixel_t     *pixel_memory,
    const size_t     pix_mem_size
);

static inline
size_t
twc_helper_jpeg_dimensions(
    const char      *jpeg_name,
    wld_dimension_t *width,
    wld_dimension_t *height,
    wld_dimension_t *stride
)  {
    return (
        twc_jpeg_file_operation(
            JA_dimensions,
            jpeg_name,
            width,
            height,
            stride,
            NULL,
            0
        )
    );
}

static inline
size_t
twc_helper_jpeg_to_buffer(
    const char  *jpeg_name,
    wld_pixel_t *pixel_memory,
    const size_t pix_mem_size
) {
    wld_dimension_t width;
    wld_dimension_t height;
    wld_dimension_t stride;

    return (
        twc_jpeg_file_operation(
             JA_read_to_buffer,
             jpeg_name,
            &width,
            &height,
            &stride,
             pixel_memory,
             pix_mem_size
        )
    );
}

 // ------------------------
 // PNG File Helper Routines

extern
size_t
twc_helper_png_dimensions(
    const char      *png_name,
    wld_dimension_t *width,
    wld_dimension_t *height,
    wld_dimension_t *stride
);

extern
size_t
twc_helper_png_to_buffer(
    const char  *png_name,
    wld_pixel_t *pixel_memory,
    size_t       pix_mem_size
);

 // ------------------------
 // SVG File Helper Routines
 //
 //   Since svg is scalable, we can choose the dimensions.

extern
size_t
twc_helper_svg_to_buffer(
    const char           *svg_name,
    const wld_dimension_t width,
    const wld_dimension_t height,
    wld_pixel_t          *pixel_memory,
    const size_t          pix_mem_size
);

#endif // TWC_IMAGE_FILE_HELPER_H

#ifndef WL_DEVAULT_H
#define WL_DEVAULT_H

 /* ---------------------------
 ** wl_shm_pool helper routines
 **
 ** https://wayland-book.com/surfaces/shared-memory.html
 ** "The following boilerplate may be freely used under public domain or CC0:"
 **
 ** This code has been modified.
 */

  // System
#include <stdlib.h>     // size_t

extern
int  // file descriptor for POSIX shared memory object
create_shm_file(
    size_t size
);

extern
void
close_shm_file(
    int fd
);


extern
void *
mmap_shm_pool(
    int    fd,
    size_t shm_size
);

extern
void
munmap_shm_pool(
    void   *addr,
    size_t  shm_size
);

#endif // WL_DEVAULT_H

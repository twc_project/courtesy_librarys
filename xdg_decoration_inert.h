#ifndef XDG_DECORATION_INERT_H
#define XDG_DECORATION_INERT_H

  // System
#include <stdint.h> // uint32_t

  // Protocols
#include <xdg-decoration-unstable-v1-client-protocol.h>

 /*
 ** For each xdg_decoration interface, declare an inert listener
 ** together with a set of correspoding inert listener routines.
 */

 // --------------
 // xdg_decoration

extern
void
inert_xdg_decoration_event_configure(
    void                               *data,
    struct zxdg_toplevel_decoration_v1 *xdg_decoration,
    uint32_t                            mode
);

extern const struct zxdg_toplevel_decoration_v1_listener inert_xdg_decoration_listener;
#define INERT_ZXDG_TOPLEVEL_DECORATION_LISTENER (&inert_xdg_decoration_listener)

#endif // XDG_DECORATION_INERT_H

# The TWC Courtesy Libraries

This repository contains two libraries that may be used while building
Wayland clients.

The first library, libtwc-util, contains stateless routines for

  - toml parser and toml tree iterators

  - XParseGeometry() of the form 'width x height + x_offset + y_offset'

  - twl_widgets for drawing a few trivial shapes[^1]

  - wl_DeVault tools for POSIX shared memory objects

  - inert (no-op) event routines and listeners for various Wayland
    protocols

The second library, libtwc-helper, consists of wrapper routines that
bundle common sequences of client requests.  These routines pass state
information as an input and/or output, but do not store any state.
Each example below represents several routines which address related
activities.

  - twc_helper_connect()

  - twc_shm_helper_create_pool()

  - twc_helper_cursor_create()

  - twc_helper_surface_create()

These helper routines come in two flavors.

  - libtwc-helper-std

  - libtwc-helper-bic

The first is for use by standard (external) clients.  The second is
for use by built-in (internal) clients.  See the
[TWC_Project](https://gitlab.com/twc_project) for more context.

## Build Instructions

````
    mkdir stage
    mkdir build

    PREFIX=stage

    PKG_CONFIG_DIR="${PREFIX}/lib/x86_64-linux-gnu/pkgconfig"
    PKG_CONFIG_PATH="${PKG_CONFIG_DIR}:${PREFIX}/share/pkgconfig"

    meson setup --prefix ${PREFIX} \
                 -Dpkg_config_path=${PKG_CONFIG_PATH} build

    cd build
    ninja -v
    meson install
````

[^1]: The TWC Project strives to use trivial graphics to avoid
      complexity in sample code.  Neither the Cairo nor Pango graphics
      libraries are used.

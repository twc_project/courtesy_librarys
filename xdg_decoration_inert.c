
  // Protocols
#include <xdg-decoration-unstable-v1-client-protocol.h>

  // APIs
#include "xdg_decoration_inert.h"   // Our API to ensure consistancy

extern
void
inert_xdg_decoration_event_configure(
    void                               *data,
    struct zxdg_toplevel_decoration_v1 *xdg_decoration,
    uint32_t                            mode
) { /* no-op */ return; }

const struct zxdg_toplevel_decoration_v1_listener inert_xdg_decoration_listener = {
    .configure = inert_xdg_decoration_event_configure,
};

#ifndef WAYLAND_INERT_H
#define WAYLAND_INERT_H

  // System
#include <stdint.h> // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

  // Protocols
#include <wayland-client-protocol.h>    // struct wl_<interface>
                                        // struct wl_<interface>_listener
 /*
 ** For each Wayland interface, declare an inert listener together
 ** with a set of correspoding inert listener routines.
 */

 // ----------
 // wl_display

extern
void
inert_wl_display_event_error(
    void              *data,
    struct wl_display *wl_display,
    void              *object_id,
    uint32_t           code,
    const char        *message
);

extern
void
inert_wl_display_event_delete_id(
    void              *data,
    struct wl_display *wl_display,
    uint32_t           id
);

extern
const
struct wl_display_listener inert_wl_display_listener;
#define INERT_WL_DISPLAY_LISTENER (&inert_wl_display_listener)

 // -----------
 // wl_registry

extern
void
inert_wl_registry_event_global(
    void               *data,
    struct wl_registry *wl_registry,
    uint32_t            name,
    const char         *interface,
    uint32_t            version
);

extern
void
inert_wl_registry_event_global_remove(
    void               *data,
    struct wl_registry *wl_registry,
    uint32_t            name
);

extern
const
struct wl_registry_listener inert_wl_registry_listener;
#define INERT_WL_REGISTRY_LISTENER (&inert_wl_register_listener)

 // ------
 // wl_shm

extern
void
inert_wl_shm_event_format(
    void          *data,
    struct wl_shm *wl_shm,
    uint32_t       format
);

extern
const
struct wl_shm_listener inert_wl_shm_listener;
#define INERT_WL_SHM_LISTENER (&inert_wl_shm_listener)

 // ---------
 // wl_buffer

extern
void
inert_wl_buffer_event_release(
    void             *data,
    struct wl_buffer *wl_buffer
);

extern
const
struct wl_buffer_listener inert_wl_buffer_listener;
#define INERT_WL_BUFFER_LISTENER (&inert_wl_buffer_listener)

 // ----------
 // wl_surface

extern
void
inert_wl_surface_event_enter(
    void              *data,
    struct wl_surface *wl_surface,
    struct wl_output  *output
);

extern
void
inert_wl_surface_event_leave(
    void              *data,
    struct wl_surface *wl_surface,
    struct wl_output  *output
);

extern
const
struct wl_surface_listener inert_wl_surface_listener;
#define INERT_WL_SURFACE_LISTENER (&inert_wl_surface_listener)


 // -------
 // wl_seat

extern
void
inert_wl_seat_event_capabilities(
    void           *data,
    struct wl_seat *wl_seat,
    uint32_t        capabilities
);

extern
void
inert_wl_seat_event_name(
    void           *data,
    struct wl_seat *wl_seat,
    const char     *name
);

extern
const
struct wl_seat_listener inert_wl_seat_listener;
#define INERT_WL_SEAT_LISTENER (&inert_wl_seat_listener)

 // ----------
 // wl_pointer

extern
void
inert_wl_pointer_event_enter(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *surface,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
);

extern
void
inert_wl_pointer_event_leave(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct wl_surface *surface
);

extern
void
inert_wl_pointer_event_motion(
    void              *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    wl_fixed_t         surface_x,
    wl_fixed_t         surface_y
);

extern
void
inert_wl_pointer_event_button(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    uint32_t           time,
    uint32_t           button,
    uint32_t           state
);

extern
void
inert_wl_pointer_event_axis(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           axis,
    wl_fixed_t         value
);

extern
void
inert_wl_pointer_event_frame(
    void *data,
    struct wl_pointer *wl_pointer
);

extern
void
inert_wl_pointer_event_axis_source(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           axis_source
);

extern
void
inert_wl_pointer_event_axis_stop(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           time,
    uint32_t           axis
);

extern
void
inert_wl_pointer_event_axis_discrete(
    void *data,
    struct wl_pointer *wl_pointer,
    uint32_t           axis,
    int32_t            discrete
);

extern
const
struct wl_pointer_listener inert_wl_pointer_listener;
#define INERT_WL_POINTER_LISTENER (&inert_wl_pointer_listener)

 // -----------
 // wl_keyboard

extern
void
inert_wl_keyboard_event_keymap(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            format,
    int32_t             fd,
    uint32_t            size
);

extern
void
inert_wl_keyboard_event_enter(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    struct wl_surface  *wl_surface,
    struct wl_array    *keys
);

extern
void
inert_wl_keyboard_event_leave(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    struct wl_surface  *wl_surface
);

extern
void
inert_wl_keyboard_event_key(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    uint32_t            time,
    uint32_t            key,
    uint32_t            state
);

extern
void
inert_wl_keyboard_event_modifiers(
    void *data,
    struct wl_keyboard *wl_keyboard,
    uint32_t            serial,
    uint32_t            mods_depressed,
    uint32_t            mods_latched,
    uint32_t            mods_locked,
    uint32_t            group
);

extern
void
inert_wl_keyboard_event_repeat_info(
    void *data,
    struct wl_keyboard *wl_keyboard,
    int32_t             rate,
    int32_t             delay
);

extern
const
struct wl_keyboard_listener inert_wl_keyboard_listener;
#define INERT_WL_KEYBOARD_LISTENER (&inert_wl_keyboard_listener)

 // ---------
 // wl_output

extern
void
inert_wl_output_event_geometry(
    void *data,
    struct wl_output *wl_output,
    int32_t           x,
    int32_t           y,
    int32_t           physical_width,
    int32_t           physical_height,
    int32_t           subpixel,
    const char       *make,
    const char       *model,
    int32_t           transform
);

extern
void
inert_wl_output_event_mode(
    void *data,
    struct wl_output *wl_output,
    uint32_t          flags,
    int32_t           width,
    int32_t           height,
    int32_t           refresh
);

extern
void
inert_wl_output_event_done(
    void *data,
    struct wl_output *wl_output
);

extern
void
inert_wl_output_event_scale(
    void *data,
    struct wl_output *wl_output,
    int32_t           factor
);

extern
void
inert_wl_output_event_name(
    void *data,
    struct wl_output *wl_output,
    const  char      *name
);

extern
void
inert_wl_output_event_description(
    void *data,
    struct wl_output *wl_output,
    const  char      *description
);

extern
const
struct wl_output_listener inert_wl_output_listener;
#define INERT_WL_OUTPUT_LISTENER (&inert_wl_output_listener)

#endif // WAYLAND_INERT_H

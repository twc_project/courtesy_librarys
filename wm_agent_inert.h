#ifndef WM_AGENT_INERT_H
#define WM_AGENT_INERT_H

  // System
#include <stdint.h> // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_fixed_t

  // Protocols
#include <wm-agent-v1-client-protocol.h>

 // -----------------
 // wm_agent_base_v1

#if 0
extern
void
inert_wm_agent_base_v1_event_interactive_agent_granted(
    void  *data,
    struct wm_agent_base_v1        *wm_agent_base_v1,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
);

extern
void
inert_wm_agent_base_v1_event_menu_agent_granted(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1,
    struct xx_wm_menu_agent *xx_wm_menu_agent
);

extern
void
inert_wm_agent_base_v1_event_decoration_stylist_granted(
    void  *data,
    struct wm_agent_base_v1         *wm_agent_base_v1,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);

extern
void
inert_wm_agent_base_v1_event_icon_stylist_granted(
    void  *data,
    struct wm_agent_base_v1   *wm_agent_base_v1,
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
);

extern
void
inert_wm_agent_base_v1_event_workspace_agent_granted(
    void  *data,
    struct wm_agent_base_v1      *wm_agent_base_v1,
    struct xx_wm_workspace_agent *xx_wm_workspace_agent
);

extern
void
inert_wm_agent_base_v1_event_task_agent_granted(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1,
    struct xx_wm_task_agent *xx_wm_task_agent
);
#endif

extern
void
inert_wm_agent_base_v1_event_new_agent_action(
    void  *data,
    struct wm_agent_base_v1       *wm_agent_base_v1,
    enum wm_agent_base_v1_agent_id agent_id,
    const char                    *name,
    uint32_t                       action_id
);

extern
void
inert_wm_agent_base_v1_event_new_agent(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1,
    uint32_t                 name,
    const char              *interface,
    uint32_t                 version
);

extern
void
inert_wm_agent_base_v1_event_restart(
    void  *data,
    struct wm_agent_base_v1 *wm_agent_base_v1
);

extern
const
struct wm_agent_base_v1_listener inert_wm_agent_base_v1_listener;
#define INERT_WM_AGENT_BASE_V1_LISTENER (&inert_wm_agent_base_v1_listener)

 // -----------------------
 // xx_wm_interactive_agent

extern
void
inert_xx_wm_interactive_agent_event_dismiss(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
);

extern
void
inert_xx_wm_interactive_agent_event_annex_revoked(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
);

extern
void
inert_xx_wm_interactive_agent_event_select(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
);

extern
void
inert_xx_wm_interactive_agent_event_track(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
);

extern
void
inert_xx_wm_interactive_agent_event_stop(
    void  *data,
    struct xx_wm_interactive_agent *xx_wm_interactive_agent
);

extern
const
struct xx_wm_interactive_agent_listener inert_xx_wm_interactive_agent_listener;
#define INERT_XX_WM_INTERACTIVE_AGENT_LISTENER (&inert_xx_wm_interactive_agent_listener)

 // ----------------------
 // xx_wm_decoration_stylist

extern
void
inert_xx_wm_decoration_stylist_event_dismiss(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);

#if 0
extern
void
inert_xx_wm_decoration_stylist_event_deco_style(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);

extern
void
inert_xx_wm_decoration_stylist_event_titlebar_attachment(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);

extern
void
inert_xx_wm_decoration_stylist_event_frame_thickness(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);

extern
void
inert_xx_wm_decoration_stylist_event_titlebar_thickness(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);

extern
void
inert_xx_wm_decoration_stylist_event_highlight_style(
    void  *data,
    struct xx_wm_decoration_stylist *xx_wm_decoration_stylist
);
#endif

extern
const
struct xx_wm_decoration_stylist_listener inert_xx_wm_decoration_stylist_listener;
#define INERT_XX_WM_DECOTATION_STYLIST_LISTENER (&inert_xx_wm_decoration_stylist_listener)

 // ----------------
 // xx_wm_decoration
 //
 //   xx_wm_decoration has no events

#if 0
extern
void
inert_xx_wm_decoration_event_dimension(
    void  *data,
    struct xx_wm_decoration *xx_wm_decoration,
    uint32_t                 width,
    uint32_t                 height
);

extern
const
struct xx_wm_decoration_listener inert_xx_wm_decoration_listener;
#define INERT_XX_WM_DECOTATION_LISTENER (&inert_xx_wm_decoration_listener)
#endif

 // ----------------
 // xx_wm_icon_stylist

extern
void
inert_xx_wm_icon_stylist_event_dismiss(
    void  *data,
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
);

extern
void
inert_xx_wm_icon_stylist_event_dismiss(
    void  *data,
    struct xx_wm_icon_stylist *xx_wm_icon_stylist
);

extern
const
struct xx_wm_icon_stylist_listener inert_xx_wm_icon_stylist_listener;
#define INERT_XX_WM_ICON_STYLIST_LISTENER (&inert_xx_wm_icon_stylist_listener)

 // ----------
 // xx_wm_icon

extern
void
inert_xx_wm_icon_event_xdg_toplevel_icon(
    void  *data,
    struct xx_wm_icon *xx_wm_icon,
    const  char       *icon_name
);

extern
void
inert_xx_wm_icon_event_xdg_toplevel_icon_buffer(
    void  *data,
    struct xx_wm_icon *xx_wm_icon,
    int32_t            size,
    int32_t            scale
);

extern
const
struct xx_wm_icon_listener inert_xx_wm_icon_listener;
#define INERT_XX_WM_ICON_LISTENER (&inert_xx_wm_icon_listener)

 // --------------
 // xx_wm_menu_agent

extern
void
inert_xx_wm_menu_agent_event_dismiss(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent
);

extern
void
inert_xx_wm_menu_agent_event_annex_revoked(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent
);

extern
void
inert_xx_wm_menu_agent_event_keyboard_shortcut(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 shortcut_key,
    uint32_t                 shortcut_key_state,
    uint32_t                 mods_depressed,
    uint32_t                 shortcut_mod_state
);

extern
void
inert_xx_wm_menu_agent_event_new_service(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    uint32_t                 numeric_name,
    uint32_t                 version,
    const  char             *provider_name,
    const  char             *service_name
);

/*
extern
void
inert_xx_wm_menu_agent_event_enter_output(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    struct wl_output        *wl_output
);

extern
void
inert_xx_wm_menu_agent_event_enter_workspace(
    void  *data,
    struct xx_wm_menu_agent *xx_wm_menu_agent,
    struct xx_wm_workspace  *xx_wm_workspace
);
*/

extern
const
struct xx_wm_menu_agent_listener inert_xx_wm_menu_agent_listener;
#define INERT_XX_WM_MENU_AGENT_LISTENER (&inert_xx_wm_menu_agent_listener)

 // -------------------
 // xx_wm_workspace_agent

extern
void
inert_xx_wm_workspace_agent_event_dismiss(
    void  *data,
    struct xx_wm_workspace_agent *xx_wm_workspace_agent
);

extern
void
inert_xx_wm_workspace_agent_event_configure_output_mode(
    void  *data,
    struct xx_wm_workspace_agent *xx_wm_workspace_agent,
    uint32_t                      mode
);

extern
const
struct xx_wm_workspace_agent_listener inert_xx_wm_workspace_agent_listener;
#define INERT_XX_WM_WORKSPACE_AGENT_LISTENER (&inert_xx_wm_workspace_agent_listener)

 // --------------
 // xx_wm_task_agent

extern
void
inert_xx_wm_task_agent_event_dismiss(
    void  *data,
    struct xx_wm_task_agent *xx_wm_task_agent
);

extern
const
struct xx_wm_task_agent_listener inert_xx_wm_task_agent_listener;
#define INERT_XX_WM_TASK_AGENT_LISTENER (&inert_xx_wm_task_agent_listener)

 // -------------------
 // xx_wm_window_registry

extern
void
inert_xx_wm_window_registry_event_new(
    void  *data,
    struct xx_wm_window_registry *xx_wm_window_registry,
    uint32_t                      name,
    uint32_t                      version
);

extern const
struct xx_wm_window_registry_listener inert_xx_wm_window_registry_listener;
#define INERT_XX_WM_WINDOW_REGISTRY_LISTENER (&inert_xx_wm_window_registry_listener)

 // ----------
 // xx_wm_window

extern
void
inert_xx_wm_window_event_iconify_state(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             state
);

extern
void
inert_xx_wm_window_event_display_state(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             state
);

extern
void
inert_xx_wm_window_event_app_id(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    const  char         *app_id
);

extern
void
inert_xx_wm_window_event_title(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    const  char         *title
);

extern
void
inert_xx_wm_window_event_location(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    wl_fixed_t           x,
    wl_fixed_t           y
);

extern
void
inert_xx_wm_window_event_icon_location(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    wl_fixed_t           x,
    wl_fixed_t           y
);

extern
void
inert_xx_wm_window_event_dimension(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
);

extern
void
inert_xx_wm_window_event_icon_dimension(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
);

extern
void
inert_xx_wm_window_event_occupancy(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t            occupancy
);

extern
void
inert_xx_wm_window_event_enter_output(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    struct wl_output    *output
);

extern
void
inert_xx_wm_window_event_pointer_focus(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             focus
);

extern
void
inert_xx_wm_window_event_keyboard_focus(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             focus
);

extern
void
inert_xx_wm_window_event_selection_focus(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             focus
);

extern
void
inert_xx_wm_window_event_thumbnail_max_size(
    void  *data,
    struct xx_wm_window *xx_wm_window,
    uint32_t             width,
    uint32_t             height
);

extern
void
inert_xx_wm_window_event_action(
    void  *data,
    struct xx_wm_window           *xx_wm_window,
    enum wm_agent_base_v1_agent_id agent_id,
    uint32_t                       action_id
);

extern
void
inert_xx_wm_window_event_expired(
    void  *data,
    struct xx_wm_window *xx_wm_window
);

extern
const
struct xx_wm_window_listener inert_xx_wm_window_listener;
#define INERT_XX_WM_WINDOW_LISTENER (&inert_xx_wm_window_listener)

 // ----------------------
 // xx_wm_workspace_registry

extern
void
inert_xx_wm_workspace_registry_event_new(
    void  *data,
    struct xx_wm_workspace_registry *xx_wm_workspace_registry,
    uint32_t                         name,
    uint32_t                         version
);

extern
const
struct xx_wm_workspace_registry_listener inert_xx_wm_workspace_registry_listener;
#define INERT_XX_WM_WORKSPACE_REGISTRY_LISTENER (&inert_xx_wm_workspace_registry_listener)

 // ---------------
 // xx_wm_workspace

extern
void
inert_xx_wm_workspace_event_active(
    void  *data,
    struct xx_wm_workspace *xx_wm_workspace,
    struct wl_output       *output
);

extern
void
inert_xx_wm_workspace_event_name(
    void  *data,
    struct xx_wm_workspace *xx_wm_workspace,
    const char             *output
);

extern
void
inert_xx_wm_workspace_event_expired(
    void  *data,
    struct xx_wm_workspace *xx_wm_workspace
);

extern
const
struct xx_wm_workspace_listener inert_xx_wm_workspace_listener;
#define INERT_XX_WM_WORKSPACE_LISTENER (&inert_xx_wm_workspace_listener)

 // ------------------------
 // xx_desktop_cue_controller

extern
void
inert_xx_desktop_cue_controller_event_abandon(
    void  *data,
    struct xx_desktop_cue_controller *xx_desktop_cue_controller
);

extern
const
struct xx_desktop_cue_controller_listener inert_xx_desktop_cue_controller_listener;
#define INERT_XX_DESKTOP_CUE_CONTROLLER_LISTENER (&inert_xx_desktop_cue_controller_listener)

 // -------------------------
 // xx_zwm_service_controller

extern
void
inert_xx_zwm_service_controller_event_new_service_operation(
    void  *data,
    struct xx_zwm_service_controller *xx_zwm_service_controller,
    const  char                      *name,
    uint32_t                          operation
);

extern
void
inert_xx_zwm_service_controller_event_abandon(
    void  *data,
    struct xx_zwm_service_controller *xx_zwm_service_controller
);

extern
const
struct xx_zwm_service_controller_listener inert_xx_zwm_service_controller_listener;
#define INERT_XX_ZWM_SERVICE_CONTROLLER_LISTENER (&inert_xx_zwm_service_controller_listener)

#endif // WM_AGENT_INERT_H

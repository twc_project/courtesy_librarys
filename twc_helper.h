#ifndef TWC_HELPER_H
#define TWC_HELPER_H

  // System
#include <stdint.h> // uint32_t, ...
#include <stdbool.h>

  // Protocols
#include <wayland-client-protocol.h>

  // APIs
#include <twc/twl_pixel_types.h>

  // Opaque types
struct wl_display;
struct wl_registry;
struct wl_compositor;
struct wl_shm;
struct wl_shm_pool;
struct wl_buffer;
struct wl_surface;
struct wl_pointer;
struct wl_seat;
struct wl_output;
struct wm_wm_base;
struct zxdg_decoration_manager_v1;
struct wm_agent_base_v1;
struct xx_desktop_cues_base_v1;

#define MAX_INTERFACE_NAME_LEN 32

 // --------------------------
 // Server Connection Routines
 //

 /*
 ** Upon connection, a compositor advertises a collection of global
 ** singletons to the client.  To manage these, a client may use a
 **
 **     struct wl_singletons
 **
 ** When calling twc_helper_connect(), clients pass a pointer to this
 ** struct.  The helper routine then
 **
 **   1. Creates the client-specific wl_display and the
 **      client-specific wl_registry.  These are placed in the
 **      clients wl_singletons struct.
 **
 **   2. Adds a general purpose listener to the newly created
 **      wl_registry.
 **
 ** The 2nd step supplies the clients unique wl_singletons struct as
 ** the 'void *data' parameter.  Then, when the server emits a
 ** wl_registry::global event, the general purpose listener routine
 ** will recover this client-specific reference.
 */

struct wl_singletons {

      // Global Singletons
    struct wl_display                 *wl_display;  // set by twc_helper_connect()
    struct wl_registry                *wl_registry;
    struct wl_compositor              *wl_compositor;
    struct wl_subcompositor           *wl_subcompositor;
    struct wl_shm                     *wl_shm;
    struct xdg_wm_base                *xdg_wm_base;
    struct zxdg_decoration_manager_v1 *xdg_decoration_manager;
    struct wm_agent_base_v1           *wm_agent_base_v1;
    struct xx_zwm_service_base_v1     *xx_zwm_service_base_v1;
    struct xx_desktop_cues_base_v1    *xx_desktop_cues_base_v1;

      // Client callback routines for select interfaces.
    void (*new_wl_seat_routine)  (struct wl_seat   *wl_seat);
    void (*new_wl_output_routine)(struct wl_output *wl_output);

    // Fix: what about the wl_registry.global_remove listener routine?
};

 // --------------------------
 // Server Connection Routines

extern
bool
twc_helper_connect(
    char                 *name,
    struct wl_singletons *wl_singletons

);
static inline
void
twc_helper_output_not_used(
    struct wl_output *wl_output
) { /* no-op */ return; }

static inline
void
twc_helper_seat_not_used(
    struct wl_seat *wl_seat
) { /* no-op */ return; }


 // -------------------
 // SHM Helper Routines
 //
 // Like the shared memory helper routines in wl_DeVault.c, the
 // routines
 //
 //   twc_shm_helper_pool_create()  and
 //   twc_shm_helper_unmap_pool()
 //
 // are meant to aid with shared memory management.  Unlike the
 // DeVault routines, these two have different behavoir depending on
 // whether they are invoked in an external client or in a BiC.
 //
 // In the external client case, these routines are simply a wrapper
 // for the DeVault routines.  See ./twc_shm_helper.c and
 // ./wl_DeVault.c.
 //
 // In the BiC case, the client and server are in the same address
 // space.  The above routines are part of the sever and will use
 // malloc() to create shared memory.  See the end of
 // libtwc/bic_server/wayland_request.c
 //
 // The two versions of these routines are contained in the libraries
 //
 //   libtwc-helper-std.a   for external clients
 //   libtwc.a              intrinsic part of the server
 //

struct wl_shm;
struct wl_shm_pool;

#define TWC_SHM_HELPER_GET_SHM_POOL( hp) ((hp)->wl_shm_pool)
#define TWC_SHM_HELPER_GET_POOL_BASE(hp) ((hp)->shm_pool_base)

struct twc_shm_helper_pool {
    int                 shm_pool_fd;
    void               *shm_pool_base;
    int32_t             shm_pool_size;
    struct wl_shm_pool *wl_shm_pool;
};

extern
void
twc_shm_helper_pool_create(
    struct wl_shm              *wl_shm,
    int32_t                     shm_pool_size,
    struct twc_shm_helper_pool *twc_shm_helper_pool
);

extern
void
twc_shm_helper_pool_unmap(
    struct twc_shm_helper_pool *twc_shm_helper_pool
);

 // ----------------------
 // Cursor Helper Routines

struct twc_cursor {
    struct twc_shm_helper_pool twc_shm_helper_pool;

    struct wl_buffer  *cursor_buffer;
    struct wl_surface *cursor_surface;

    uint32_t hotspot_x;
    uint32_t hotspot_y;
};

extern
int
twc_helper_lookup_Xcursor_by_name(
    char *cursor_name
);

extern
struct twc_cursor
twc_helper_cursor_create(
    struct wl_singletons *wl_singletons,
    int                   cursor_index,
    twl_color_t           foreground,
    twl_color_t           background
);

extern
void
twc_helper_cursor_destroy(
    struct twc_cursor *twc_cursor
);

extern
void
twc_helper_cursor_set(
    struct wl_pointer *wl_pointer,
    uint32_t           serial,
    struct twc_cursor *twc_cursor
);

 // -----------------------
 // Surface Helper Routines

#define TWC_HELPER_GET_SURFACE(sb) ((sb).wl_surface)

struct twc_helper_surface_basics {

    // This struct is suitable for holding the meta data needed for a
    // wl_surface which employs two buffers.  The size of each buffer
    // may be up to max_surface_size.

    int32_t max_surface_size;       // In pixels
    int32_t stated_buffer_width;    //   |
    int32_t stated_buffer_height;   //   v

      // A shm pool for pixel memory
    struct twc_shm_helper_pool twc_shm_helper_pool;

      // Two buffers.  One current and one for staging.  A wl_buffer
      // is really a pixel-memory descriptor.  The dimension, stride
      // and pixel-memory location is fixed for the life of a
      // wl_buffer.  A new replacement buffer must be created if any
      // of these attributes need updating.  The old wl_buffer should
      // be destroyed.
    struct wl_buffer *wl_buffer[2];

      // Byte offsets within wl_shm_pool memory for each buffer.  The
      // values are set to 0 and (max_surface_size * BYTES_PER_PIXEL)
      // resp.
    int32_t pixel_buffer_offset[2];

      // Index of next buffer.  The other may be in-use or free.
    uint8_t next_buffer_index;
    bool    free[2];    // released by compositor

      // Normally, buffer width/height match the stated values, but
      // not necessarily when in transition.  The free buffer may be
      // updated while the current buffer must wait until it is
      // released.
    int32_t buffer_width [2];   // In pixels
    int32_t buffer_height[2];   //   "

    struct wl_surface *wl_surface;
    struct wl_display *wl_display;
};

extern
struct wl_surface*
twc_helper_surface_create(
    struct twc_helper_surface_basics *surface_basics,
    int32_t                           max_surface_size,
    struct wl_singletons             *wl_singletons,
    const struct wl_surface_listener *wl_surface_listener,
    void                             *data
);

extern
void
twc_helper_surface_destroy(
    struct twc_helper_surface_basics *surface_basics
);

extern
bool
twc_helper_surface_update_buffers(
    struct twc_helper_surface_basics *surface_basics,
    int32_t                          new_buffer_width,
    int32_t                          new_buffer_height
);

extern
void
twc_helper_surface_release_buffer(
    struct twc_helper_surface_basics *surface_basics,
    struct wl_buffer                 *wl_buffer
);

static inline
void*
twc_helper_surface_next_pixel_memory(
    struct twc_helper_surface_basics *surface_basics
) {
    uint8_t *pool_base = (
        TWC_SHM_HELPER_GET_POOL_BASE(&surface_basics->twc_shm_helper_pool)
    );

    return (
        (void*) (
            surface_basics->next_buffer_index
            ? (pool_base + surface_basics->max_surface_size)
            :  pool_base
        )
    );
}

extern
bool
twc_helper_surface_attach_commit(
    struct twc_helper_surface_basics *surface_basics
);

#endif // TWC_HELPER_H

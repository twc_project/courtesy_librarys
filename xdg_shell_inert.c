
  // System
#include <stdint.h>         // uint32_t

  // Wayland
#include <wayland-util.h>   // wl_array

  // Protocols
#include <xdg-shell-client-protocol.h>

  // APIs
#include "xdg_shell_inert.h"    // Our API to ensure consistancy

 // ---------------------
 // xdg_surface listeners
 //

extern
void
inert_xdg_surface_event_configure(
    void               *data,
    struct xdg_surface *xdg_surface,
    uint32_t            serial
) { /* no-op */ return; }

const struct xdg_surface_listener inert_xdg_surface_listener = {
    .configure = inert_xdg_surface_event_configure,
};

 // ----------------------
 // xdg_toplevel listeners
 //

extern
void
inert_xdg_toplevel_event_configure(
    void                *data,
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height,
    struct wl_array     *states
) { /* no-op */ return; }

extern
void
inert_xdg_toplevel_event_close(
    void                *data,
    struct xdg_toplevel *xdg_toplevel
) { /* no-op */ return; }

extern
void
inert_xdg_toplevel_event_configure_bounds(
    void *data,
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height
) { /* no-op */ return; }

extern
void
inert_xdg_toplevel_event_wm_capabilities(
    void *data,
    struct xdg_toplevel *xdg_toplevel,
    struct wl_array     *capabilities
) { /* no-op */ return; }

const struct xdg_toplevel_listener inert_xdg_toplevel_listener = {
    .configure        = inert_xdg_toplevel_event_configure,
    .close            = inert_xdg_toplevel_event_close,
    .configure_bounds = inert_xdg_toplevel_event_configure_bounds,
    .wm_capabilities  = inert_xdg_toplevel_event_wm_capabilities,
};

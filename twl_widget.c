
// ----------------------------
// Trivial Widget Library (TWL)

 /*
 ** It would be nice to find a lightweight widget toolkit to render
 ** standard graphic elements such as buttons, menus etc.  For example,
 ** GTK, LVGL, IUP, and Elementary (from Enlightenment).
 **
 ** The problem is they all operate as a client program for some
 ** graphics server like Xorg or Wayland.  They want to manage images
 ** all the way down to the GPU.  I'm looking for a widget toolkit that
 ** can render a high level description of, say a menu, into the pixel
 ** buffer of a surface.  TWC can then compose this surface using the
 ** same mechanisms as all other surfaces.
 **
 **
 ** https://blog.lvgl.io/2018-01-03/linux_fb
 ** Gives an example using linux frame buffer.  Using this code.
 ** https://github.com/lvgl/lv_drivers/blob/master/display/fbdev.c
 ** Compare this code to the porting guide below.
 ** https://github.com/lvgl/lv_port_linux_frame_buffer
 **
 **
 ** https://docs.lvgl.io/latest/en/html/porting/display.html#display-buffer
 ** https://docs.lvgl.io/latest/en/html/porting/index.html
 ** Shows how to port to a "graphics device."  Do this where the
 ** "device" is a bic_buffer.
 **
 **
 ** https://docs.lvgl.io/latest/en/html/porting/index.html
 ** Hopefully input is similarly simple.  There are probably callouts
 ** for when a button is pressed.
 */

  // Enabling strnlen in glib.c requires a "feature test macro"
  // https://www.man7.org/linux/man-pages/man3/strnlen.3.html
  // https://man7.org/linux/man-pages/man7/feature_test_macros.7.html
#define _POSIX_C_SOURCE 200809L

  // System
#include <stdbool.h>
#include <stdint.h> // uint32_t
#include <string.h> // strnlen

  // APIs
#include "twl_widget.h"         // Our API to ensure consistancy
#include "twc_log.h"
#include "twc/twl_pixel_types.h"
#include "twc/wayland_types.h"  // wld_<type>_t

  // Fonts
#include "fixed_6x10.h"
#include "fixed_7x12.h"
#include "fixed_8x13.h"
#include "fixed_9x15.h"

  // Cursors
#include "cursorfont.h"
#include "cursorglyph.h"

#define threeD 2    // Two pixels of 3D sculpting

#define ASCII_SPACE  0x20
#define ASCII_DELETE 0x7f

extern
void
twl_widget_draw_button(
    twl_pixel_t          *pixel_array,
    struct twl_grid      *twl_button_grid,
    int                   row,
    int                   col,
    enum twl_button_state button_state,
    const char           *label
) {
    twl_color_t tl, br, fl; // colors for ThreeD sculpting of panel edges.
                            // tl is color for top & left sculpting.  br
                            // is color for bottom & right sculpting.
                            // fl is color for button field.

    switch ( button_state ) {
        case PRESSED:
            fl = GRAY_40;
            tl = GRAY_26;
            br = GRAY_A0;
        break;

        case NOT_PRESSED:
            fl = GRAY_40;
            tl = GRAY_A0;
            br = GRAY_26;
        break;

        case NONE:
            fl = GRAY_7F;
            tl = GRAY_BF;
            br = GRAY_4C;
        break;
    }

    int panel_width  = twl_button_grid->panel_width;
    int panel_height = twl_button_grid->panel_height;

      // Pixel coordinates of the panel's origin pixel
    int pixel_X0 = TWC_WIDGET_PANEL_ORIGIN(col, panel_width);
    int pixel_Y0 = TWC_WIDGET_PANEL_ORIGIN(row, panel_height);

    int stride = twl_button_grid->stride;

      // The offset in pixel memory of the panel's origin pixel
    int pixel_offset = pixel_Y0 * stride + pixel_X0;
    int pix_offset   = pixel_offset;

    int w;          // w indexes columns of pixels in a panel.
    int h;          // h indexes rows    of pixels in a panel.
    int e = 0;      // e counts up   for top-right   corner sculpting
    int b = threeD; // b counts down for bottom-left corner sculpting

      // Draw the first few rows with the top-right corner diagonally
      // sculpted
    for (h = 0; h < threeD; h++ ) {

          // w counts pixels in a row.  pixel_offset moves along in buffer memory.
        for ( w = 0 ; w < panel_width - e ; w++ ) { pixel_array[ pix_offset++ ] = tl; }
        for (       ; w < panel_width     ; w++ ) { pixel_array[ pix_offset++ ] = br; }

          // Advance to the next interval of pixels in buffer memory
        pix_offset += stride - panel_width;
        e++;
    }

      // Draw the bulk of the rows in the middle of the panel with
      // left and right edges sculpted
    for ( ; h < panel_height - threeD ; h++ ) {

        for ( w = 0 ; w < threeD               ; w++ ) { pixel_array[ pix_offset++ ] = tl; }
        for (       ; w < panel_width - threeD ; w++ ) { pixel_array[ pix_offset++ ] = fl; }
        for (       ; w < panel_width          ; w++ ) { pixel_array[ pix_offset++ ] = br; }

        pix_offset += stride - panel_width;
    }

      // Draw the last few rows with the bottom-left corner diagonally
      // sculpted
    for ( ; h < panel_height; h++ ) {

        for ( w = 0; w < b           ; w++ ) { pixel_array[ pix_offset++ ] = tl; }
        for (      ; w < panel_width ; w++ ) { pixel_array[ pix_offset++ ] = br; }

        pix_offset += stride - panel_width;
        b--;
    }

    if ( label != NULL ) {

        size_t len = strnlen(label, 20);

        twl_widget_render_text(
            FS_fixed_7x12,
           &pixel_array[pixel_offset],
            stride,
            panel_height,
            label,
            (panel_width  - (len * fixed_7x12_glyph_width)) / 2,
            (panel_height -        fixed_7x12_glyph_height) / 2,
            WHITE,
            fl
        );
    }

    return;
}

extern
void
twl_widget_fill_box(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  w,         // fill width
    wld_dimension_t  h,         // fill height
    wld_dimension_t  S,         // stride in pixels
    twl_color_t      color,
    wld_coordinate_t x0,
    wld_coordinate_t y0
) {
    if (( x0 < 0      ) ||
        ( y0 < 0      ) ||
        ( S  < x0 + w )
    ) { return; }

    wld_coordinate_t col_offset;
    wld_coordinate_t col_limit  =  x0    + w;
    wld_coordinate_t row_offset =  y0    * S;
    wld_coordinate_t row_limit  = (y0+h) * S;

    for (                   ; row_offset < row_limit ; row_offset += S )
        for (col_offset = x0; col_offset < col_limit ; col_offset++    ) {

        pix_buf[ row_offset + col_offset ] = color;
    }

    return;
}

extern
void
twl_widget_fill_pix_buf_all(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,
    wld_dimension_t  H,
    twl_color_t      color
) {
    twl_widget_fill_box(
        pix_buf,
        W,
        H,
        W,      // stride is width
        color,
        0, 0
    );
}

extern
void
twl_widget_draw_box(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  width,
    wld_dimension_t  height,
    uint32_t         buffer_stride,
    twl_color_t      color,
    enum twl_pattern pattern
) {
    uint32_t stipple_color = (
        pattern == STIPPLE
        ? BLACK
        : color
    );
    //LOG_INFO("twl_widget_draw_box %d %d %d", width, height, buffer_stride);

    // Fix: the pattern should alternate from col-to-col, row-to-row.

#   define COLOR(i) ((i) % 2) ? color : stipple_color

    uint32_t w;  // w indexes columns of pixels in an entry.
    uint32_t h;  // h indexes rows    of pixels in an entry.

      // First row, do all pixels
    for (h = 0; h < 1; h++ ) {
        for ( w = 0 ; w < width; w++ ) { *pix_buf++ = COLOR(w); }
    }

    pix_buf += buffer_stride - width;

      // Middle rows, do first and last pixel
    for (     ; h < height - 1; h++ ) {
        *pix_buf++ =  COLOR(h);
         pix_buf   += width - 2;
        *pix_buf++ =  COLOR(h);
         pix_buf   += buffer_stride - width;
    }

      // Last row, do all pixels
    for ( w = 0 ; w < width; w++ ) { *pix_buf++ = COLOR(w); }

    return;
}

static inline
int
glyph_to_bit_6x10(
    char            c,
    wld_dimension_t glyph_row,
    wld_dimension_t glyph_col
) {
    return fixed_6x10_bit_map[c - ASCII_SPACE][glyph_row] & (1 << glyph_col);
}

static inline
int
glyph_to_bit_7x12(
    char            c,
    wld_dimension_t glyph_row,
    wld_dimension_t glyph_col
) {
    return fixed_7x12_bit_map[c - ASCII_SPACE][glyph_row] & (1 << glyph_col);
}

static inline
int
glyph_to_bit_8x13(
    char            c,
    wld_dimension_t glyph_row,
    wld_dimension_t glyph_col
) {
    return fixed_8x13_bit_map[c - ASCII_SPACE][glyph_row] & (1 << glyph_col);
}

static inline
int
glyph_to_bit_9x15(
    char            c,
    wld_dimension_t glyph_row,
    wld_dimension_t glyph_col
) {
    return fixed_9x15_bit_map[c - ASCII_SPACE][glyph_row] & (1 << glyph_col);
}

static inline
int
glyph_to_bit_x_cursors(
    int             glyph_index,
    wld_dimension_t glyph_row,
    wld_dimension_t glyph_col
) {
    return x_cursors_bit_map[glyph_index][glyph_row] & (1 << glyph_col);
}

static
wld_dimension_t
twl_widget_render_text_6x10(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,      // pix_buf width == pix_buf stride
    wld_dimension_t  H,      // pix_buf height
    const char      *string,
    wld_coordinate_t x0,     // pix_buf relative string_loc_x
    wld_coordinate_t y0,     // pix_buf relative string_loc_y
    twl_color_t      foreground,
    twl_color_t      background
) {
    // The pix_buf offset for the upper leftmost pixel of string[0] is
    //
    //     pix_buf[ (y0 * W) + x0 ]
    //
    // Sucessive rows increment by W, columns by one.

    wld_dimension_t glyph_width  = fixed_6x10_glyph_width;
    wld_dimension_t glyph_height = fixed_6x10_glyph_height;

    if (( 4 * 1920 <= W            ) ||  // sanity check, <= a pair of 4k monitors
        ( H - y0   <  glyph_height )     // don't overflow pix_buf vertically
    ) { return 0; }

    wld_dimension_t Y = y0 * W;
    wld_dimension_t X;

      // The number of rows in the string is the font height
    for (wld_dimension_t glyph_row = 0; glyph_row < glyph_height; glyph_row++) {

        X = x0;

        for ( const char *c = string; *c != '\0'; c++) {
            if ( W < X + glyph_width ) break;    // don't overflow pix_buf horizontally

              // char is an unsigned type  -128 <= char < 128.
              // The unprintable chars are -128 <= char <  32.
              // Unprintable chars display as delete.
            char glyph_index = *c < ASCII_SPACE ? ASCII_DELETE : *c;

            for (wld_dimension_t glyph_col = 0; glyph_col < glyph_width;  glyph_col++) {
                pix_buf[ Y + X++ ] = (
                    glyph_to_bit_6x10(glyph_index, glyph_row, glyph_col)
                    ? foreground
                    : background
                );
            }
        }
        Y += W;
    }

    return X;
}

static
wld_dimension_t
twl_widget_render_text_7x12(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,      // pix_buf width == pix_buf stride
    wld_dimension_t  H,      // pix_buf height
    const char      *string,
    wld_coordinate_t x0,     // pix_buf relative string_loc_x
    wld_coordinate_t y0,     // pix_buf relative string_loc_y
    twl_color_t      foreground,
    twl_color_t      background
) {
    wld_dimension_t glyph_width  = fixed_7x12_glyph_width;
    wld_dimension_t glyph_height = fixed_7x12_glyph_height;

    if (( 4 * 1920 <= W            ) ||  // sanity check, <= a pair of 4k monitors
        ( H - y0   <  glyph_height )     // don't overflow pix_buf vertically
    ) { return 0; }

    wld_dimension_t Y = y0 * W;
    wld_dimension_t X;

      // The number of rows in the string is the font height
    for (wld_dimension_t glyph_row = 0; glyph_row < glyph_height; glyph_row++) {

        X = x0;

        for ( const char *c = string; *c != '\0'; c++) {
            if ( W < X + glyph_width ) break;    // don't overflow pix_buf horizontally

              // char is an unsigned type  -128 <= char < 128.
              // The unprintable chars are -128 <= char <  32.
              // Unprintable chars display as delete.
            char glyph_index = *c < ASCII_SPACE ? ASCII_DELETE : *c;

            for (wld_dimension_t glyph_col = 0; glyph_col < glyph_width;  glyph_col++) {
                pix_buf[ Y + X++ ] = (
                    glyph_to_bit_7x12(glyph_index, glyph_row, glyph_col)
                    ? foreground
                    : background
                );
            }
        }
        Y += W;
    }

    return X;
}

static
wld_dimension_t
twl_widget_render_text_8x13(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,      // pix_buf width == pix_buf stride
    wld_dimension_t  H,      // pix_buf height
    const char      *string,
    wld_coordinate_t x0,     // pix_buf relative string_loc_x
    wld_coordinate_t y0,     // pix_buf relative string_loc_y
    twl_color_t      foreground,
    twl_color_t      background
) {
    wld_dimension_t glyph_width  = fixed_8x13_glyph_width;
    wld_dimension_t glyph_height = fixed_8x13_glyph_height;

    if (( 4 * 1920 <= W            ) ||  // sanity check, <= a pair of 4k monitors
        ( H - y0   <  glyph_height )     // don't overflow pix_buf vertically
    ) { return 0; }

    wld_dimension_t Y = y0 * W;
    wld_dimension_t X;

      // The number of rows in the string is the font height
    for (wld_dimension_t glyph_row = 0; glyph_row < glyph_height; glyph_row++) {

        X = x0;

        for ( const char *c = string; *c != '\0'; c++) {
            if ( W < X + glyph_width ) break;    // don't overflow pix_buf horizontally

              // char is an unsigned type  -128 <= char < 128.
              // The unprintable chars are -128 <= char <  32.
              // Unprintable chars display as delete.
            char glyph_index = *c < ASCII_SPACE ? ASCII_DELETE : *c;

            for (wld_dimension_t glyph_col = 0; glyph_col < glyph_width;  glyph_col++) {
                pix_buf[ Y + X++ ] = (
                    glyph_to_bit_8x13(glyph_index, glyph_row, glyph_col)
                    ? foreground
                    : background
                );
            }
        }
        Y += W;
    }

    return X;
}

static
wld_dimension_t
twl_widget_render_text_9x15(
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,      // pix_buf width == pix_buf stride
    wld_dimension_t  H,      // pix_buf height
    const char      *string,
    wld_coordinate_t x0,     // pix_buf relative string_loc_x
    wld_coordinate_t y0,     // pix_buf relative string_loc_y
    twl_color_t      foreground,
    twl_color_t      background
) {
    wld_dimension_t glyph_width  = fixed_9x15_glyph_width;
    wld_dimension_t glyph_height = fixed_9x15_glyph_height;

    if (( 4 * 1920 <= W            ) ||  // sanity check, <= a pair of 4k monitors
        ( H - y0   <  glyph_height )     // don't overflow pix_buf vertically
    ) { return 0; }

    wld_dimension_t Y = y0 * W;
    wld_dimension_t X;

      // The number of rows in the string is the font height
    for (wld_dimension_t glyph_row = 0; glyph_row < glyph_height; glyph_row++) {

        X = x0;

        for ( const char *c = string; *c != '\0'; c++) {
            if ( W < X + glyph_width ) break;    // don't overflow pix_buf horizontally

              // char is an unsigned type  -128 <= char < 128.
              // The unprintable chars are -128 <= char <  32.
              // Unprintable chars display as delete.
            char glyph_index = *c < ASCII_SPACE ? ASCII_DELETE : *c;

            for (wld_dimension_t glyph_col = 0; glyph_col < glyph_width;  glyph_col++) {
                pix_buf[ Y + X++ ] = (
                    glyph_to_bit_9x15(glyph_index, glyph_row, glyph_col)
                    ? foreground
                    : background
                );
            }
        }
        Y += W;
    }

    return X;
}

extern
wld_dimension_t
twl_widget_render_text(
    enum font_size   font_size,
    twl_pixel_t     *pix_buf,
    wld_dimension_t  W,      // pix_buf width == pix_buf stride
    wld_dimension_t  H,      // pix_buf height
    const char      *string,
    wld_coordinate_t x0,     // pix_buf relative string_loc_x
    wld_coordinate_t y0,     // pix_buf relative string_loc_y
    twl_color_t      foreground,
    twl_color_t      background
) {
    switch ( font_size ) {
        case FS_fixed_6x10:
            twl_widget_render_text_6x10(
                pix_buf,
                W,
                H,
                string,
                x0,
                y0,
                foreground,
                background
            );
        break;

        default:
        case FS_fixed_7x12:
            twl_widget_render_text_7x12(
                pix_buf,
                W,
                H,
                string,
                x0,
                y0,
                foreground,
                background
            );
        break;

        case FS_fixed_8x13:
            twl_widget_render_text_8x13(
                pix_buf,
                W,
                H,
                string,
                x0,
                y0,
                foreground,
                background
            );
        break;

        case FS_fixed_9x15:
            twl_widget_render_text_9x15(
                pix_buf,
                W,
                H,
                string,
                x0,
                y0,
                foreground,
                background
            );
        break;
    }

    return 1;
}

twl_pixel_t wayland_logo[256] = {
#   include "wayland_logo_16x16.h"
};

extern
XC_hotspot_t
twl_widget_render_X_cursor(
    twl_pixel_t *pix_buf,
    int          cursor_index,
    twl_color_t  foreground,
    twl_color_t  background
) {
      // Special case for the wayland logo.  It really isn't one of
      // the X cursors.  It's never rendered in reverse video.
    if ( cursor_index == XC_wayland_logo ) {
        for (int i = 0; i < 256; i++) pix_buf[ i ] = wayland_logo[i];
        return 0x78;    // wayland logo hotspot
    }

    if ( XC_num_glyphs <= cursor_index ) { return XC_HOTSPOT_INVALID; }

      // If n is a cursor index, its corresponding mask index is n+1.
      // Therefore, cursor indices are even.
    if ( cursor_index % 2 ) { return XC_HOTSPOT_INVALID; }

    // The pix_buf offset for the upper leftmost pixel of string[0] is
    //
    //     pix_buf[ (y0 * W) + x0 ]
    //
    // Sucessive rows increment by W, columns by one.

    wld_dimension_t glyph_width  = x_cursors_glyph_width;
    wld_dimension_t glyph_height = x_cursors_glyph_height;

    wld_dimension_t Y = 0;
    wld_dimension_t X;

    int cursor_bit, mask_bit;
    twl_color_t  color;

    /*
    ** For the X cursor fonts, each mask bit is established by two
    ** tests.  1.) It is set for each pixel that is in the cursor
    ** image.  2.) It is also set for each pixel that "outlines" the
    ** cursor image.  See
    **
    **     https://tronche.com/gui/x/xlib/appendix/b/
    **
    ** This is useful for "reverse video" rendering.
    */

      // The number of rows in the string is the font height
    for (wld_dimension_t glyph_row = 0; glyph_row < glyph_height; glyph_row++) {

        X = 0;

        for (wld_dimension_t glyph_col = 0; glyph_col < glyph_width;  glyph_col++) {

            cursor_bit = glyph_to_bit_x_cursors(cursor_index,   glyph_row, glyph_col);
            mask_bit   = glyph_to_bit_x_cursors(cursor_index+1, glyph_row, glyph_col);

            if (cursor_bit) {
                if (mask_bit) {
                    color = foreground;
                } else {
                    color = foreground;
                }
            } else {
                if (mask_bit) {
                    color = background;
                } else {
                    color = TRANSPARENT_BLACK;
                }
            }

            pix_buf[ Y + X++ ] = color;

        }
        Y += glyph_width;
    }

    return XC_cursor_hotspots[cursor_index / 2];
}

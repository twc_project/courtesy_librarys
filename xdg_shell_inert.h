#ifndef XDG_SHELL_INERT_H
#define XDG_SHELL_INERT_H

  // System
#include <stdint.h> // uint32_t

  // Protocols
#include <xdg-shell-client-protocol.h>

 /*
 ** For each xdg_shell interface, declare an inert listener together
 ** with a set of correspoding inert listener routines.
 */

 // -----------
 // xdg_surface

extern
void
inert_xdg_surface_event_configure(
    void               *data,
    struct xdg_surface *xdg_surface,
    uint32_t            serial
);

extern const struct xdg_surface_listener inert_xdg_surface_listener;
#define INERT_XDG_SURFACE_LISTENER (&inert_xdg_surface_listener)

 // ------------
 // xdg_toplevel

extern
void
inert_xdg_toplevel_event_configure(
    void                *data,
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height,
    struct wl_array     *states
);

extern
void
inert_xdg_toplevel_event_close(
    void                *data,
    struct xdg_toplevel *xdg_toplevel
);

extern
void
inert_xdg_toplevel_event_configure_bounds(
    void *data,
    struct xdg_toplevel *xdg_toplevel,
    int32_t              width,
    int32_t              height
);

extern
void
inert_xdg_toplevel_event_wm_capabilities(
    void *data,
    struct xdg_toplevel *xdg_toplevel,
    struct wl_array     *capabilities
);



extern const struct xdg_toplevel_listener inert_xdg_toplevel_listener;
#define INERT_XDG_TOPLEVEL_LISTENER (&inert_xdg_toplevel_listener)

#endif  // XDG_SHELL_INERT_H

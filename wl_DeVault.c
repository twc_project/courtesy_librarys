
  // Add the nsec member to struct timespec in time.h
#define _POSIX_C_SOURCE 200112L

#define _GNU_SOURCE

  // System
#include <errno.h>
#include <sys/mman.h>   // shm_open(), shm_unlink(), mmap(), munmap(), size_t()
#include <fcntl.h>      // O_RDWR, ...
#include <unistd.h>     // ftruncate(), close()
#include <time.h>       // clock_gettime()

  // APIs
#include "wl_DeVault.h" // Our API to ensure consistancy

 // ----------------------------------
 // Unix shared memory helper routines
 //
 // https://wayland-book.com/surfaces/shared-memory.html
 // "The following boilerplate may be freely used under public domain or CC0:"
 //
 // This code has been modified.
 //
 // To create a wl_shm_pool, clients typically use some variation of
 // the following Unix system calls:
 //
 //     int fd     = shm_open (...);
 //     int ret    = ftruncate(fd, size)
 //     void *data = mmap     (..., size, ..., fd, ...);
 //
 // The file descriptor (fd) is then passed from client to server as a
 // means of establishing a shared memory segment (wl_shm_pool).

static void
randname(char *buf)
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    long r = ts.tv_nsec;
    for (int i = 0; i < 6; ++i) {
        buf[i] = 'A'+(r&15)+(r&16)*2;
        r >>= 5;
    }
}

static int
open_shm_file(void)
{
    int retries = 100;
    do {
        char name[] = "/wl_shm-XXXXXX";
        randname(name + sizeof(name) - 7);
        --retries;
        int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
        if (fd >= 0) {
            shm_unlink(name);
            return fd;
        }
    } while (retries > 0 && errno == EEXIST);
    return -1;
}

extern int
create_shm_file(size_t size)
{
    int fd = open_shm_file();
    if (fd < 0)
        return -1;
    int ret;
    do {
        ret = ftruncate(fd, size);
    } while (ret < 0 && errno == EINTR);
    if (ret < 0) {
        close(fd);
        return -1;
    }
    return fd;
}

extern void
close_shm_file(
    int     fd
) {
    close (fd);

    return;
}

extern void*
mmap_shm_pool(
    int    fd,
    size_t shm_size
) {
    void *data = mmap(
        NULL,
        shm_size,
        PROT_READ | PROT_WRITE, MAP_SHARED,
        fd,
        0
    );
    if (data == MAP_FAILED) {
        close(fd);
        return NULL;
    }

    return data;
}

extern void
munmap_shm_pool(
    void   *addr,
    size_t  shm_size
) {
    munmap(addr, shm_size);

    return;
}

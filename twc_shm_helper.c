
  // System
#include <stdint.h>     // uint32_t, ...
#include <unistd.h>     // close()
#include <sys/mman.h>   // munmap()

  // APIs
#include "twc_helper.h"     // Our API to ensure consistancy
#include "wl_DeVault.h"

 /*
 ** These twc_shm_helper routines are intended to aid clients with shm
 ** pool management.  These routines have two versions, one for
 ** external clients and one for BiCs.  The external version (found
 ** here) uses the wl_DeVault helpers (shm_open and mmap) to establish
 ** shared memory.  The internal BiC version uses malloc() for shared
 ** memory.  See also twc_helper.h.
 **
 ** This code is available in libtwc-helper-std.a
 */

extern
void
twc_shm_helper_pool_create(
    struct wl_shm              *wl_shm,
    int32_t                     shm_pool_size,
    struct twc_shm_helper_pool *twc_shm_helper_pool
) {
    int shm_pool_fd = (
        create_shm_file( shm_pool_size )
    );

    void *shm_pool_base = (
        mmap_shm_pool( shm_pool_fd, shm_pool_size )
    );

    struct wl_shm_pool *wl_shm_pool = (
        wl_shm_create_pool(
            wl_shm,
            shm_pool_fd,
            shm_pool_size
        )
    );

    twc_shm_helper_pool->shm_pool_fd   = shm_pool_fd;
    twc_shm_helper_pool->shm_pool_base = shm_pool_base;
    twc_shm_helper_pool->shm_pool_size = shm_pool_size;
    twc_shm_helper_pool->wl_shm_pool   = wl_shm_pool;

    close(shm_pool_fd);

    return;
}

extern
void
twc_shm_helper_pool_unmap(
    struct twc_shm_helper_pool *twc_shm_helper_pool
) {
    // The shm_pool type can only be SPT_MMAP.  The shared memory must
    // be unmapped.

    munmap(
        twc_shm_helper_pool->shm_pool_base,
        twc_shm_helper_pool->shm_pool_size
    );

    return;
}

#ifndef WM_SERVICE_INERT_H
#define WM_SERVICE_INERT_H

  // Protocols
#include <wm-service-v1-client-protocol.h>

 // ----------------------
 // xx_zwm_service_base_v1
 //
 //   There are no events

 // --------------
 // xx_zwm_service

extern
void
inert_xx_zwm_service_event_operation(
    void *data,
    struct xx_zwm_service *xx_zwm_service,
    uint32_t               operation
);

extern
const
struct xx_zwm_service_listener inert_xx_zwm_service_listener;
#define INERT_WM_SERVICE_LISTENER (&inert_xx_zwm_service_listener)

 // -------------------
 // xx_zwm_service_window
 //
 //   There are no events

#endif // WM_SERVICE_INERT_H

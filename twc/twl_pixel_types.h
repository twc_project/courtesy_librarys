#ifndef TWL_PIXEL_TYPES_H
#define TWL_PIXEL_TYPES_H

#define BYTES_PER_PIXEL 4

typedef uint32_t  twl_color_t;  // 32 bits/pixel RGB
typedef uint32_t  twl_pixel_t;  // 32 bits/pixel RGB

#endif  // TWL_PIXEL_TYPES_H

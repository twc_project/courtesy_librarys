#ifndef WAYLAND_TYPES_H
#define WAYLAND_TYPES_H

  // System
#include <stdint.h>  // uint32_t

  /*
  ** In Wayland, outputs (aka a screen, display or monitor) are
  ** logically positioned relative to one another within a single,
  ** global, rectangular, coordinate-system called the Layout.  By
  ** convention (0,0), i.e., the origin, is located at the upper left
  ** corner with positive x toward the right and positive y toward the
  ** bottom.
  **
  ** The coordinates of a location (say mouse pointer) within the
  ** global system, are called the layout coordinates of the location.
  ** By construction, the x and y values in layout coordinates are
  ** always positive.
  **
  ** Suppose that the origin point had been specified at some other
  ** point besides the upper left.  In this case, every location would
  ** still have coordinates, but the x or y value could be negative.
  ** A simple translation formula could be used to convert coordinates
  ** between a system using an upper/left origin and some other
  ** origin.
  **
  ** Sometimes it is convenient to operate as if the origin point
  ** actually had been located somewhere else.  For example, when
  ** dealing with a specific output, it's useful to think of the
  ** origin as if it were at the upper left corner of that output.  In
  ** this case, every location in the global system could be assigned
  ** a translated coordinate using this alternate origin.  We say
  ** these are output coordinates.  If a location happens to be
  ** visible on an output display, then its output coordinates are
  ** bounded by 0 and the dimensions of the output.
  **
  ** Below are the most common alternate origins:
  **
  **     layout    coordinates,  origin at global upper left
  **     ouput     coordinates,  origin at upper left of output
  **     window    coordinates,  origin at upper left of toplevel
  **     surface   coordinates,  origin at upper left of surface   (*)
  **
  ** (*) Some toplevels are a complex of many sufaces.
  */

  /*
  ** See
  ** https://wayland.freedesktop.org/docs/html/ch04.html#sect-Protocol-Wire-Format
  */

  /*
  ** Wayland uses 32 bits for integral types.  E.g., for buffer width
  ** & height; for coordinates; for output position; for surface width
  ** & height; and timing.
  **
  ** Wayland uses wl_fixed_t type for wl_pointer motion.  wl_fixed_t
  ** is a 32 bit fixed-point number representation with a 24.8 format.
  */

  // Coordinates may be negative, but dimensions never are.
typedef  int32_t  wld_coordinate_t;
typedef uint32_t  wld_dimension_t;
typedef uint32_t  wld_pixel_t;  // Wayland Protocol requires "All
                                // renderers should support argb8888
                                // and xrgb8888 ..."  Ie, 32 bit

#define WLD_INVALID_COORD ((wld_coordinate_t) INT32_MIN)  // 0 is valid, use min int instead.

typedef struct {
    wld_coordinate_t x;
    wld_coordinate_t y;

    wld_dimension_t  width;
    wld_dimension_t  height;
} wld_geometry_t;

  // Wayland timing is unsigned and has millisecond granularity.
typedef uint32_t wld_millisecond_t;

#define ADD_LISTENER_SUCCESS   0
#define ADD_LISTENER_FAILURE (-1)

typedef  int32_t wld_int_t;
typedef uint32_t wld_uint_t;

#endif  // WAYLAND_TYPES_H

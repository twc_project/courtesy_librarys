#ifndef TOML_ITERATORS_H
#define TOML_ITERATORS_H

  // TWC
#include <twc/toml.h>

 // Macros for creating unique variable names.
 //
 // An iterator macro which declares a variable name can only be used
 // once within a file.  Otherwise, multiple declarations will occur.
 // The following helper macros use __LINE__ to create variable names
 // based on line number.
 //
 // Example: consider the following 'for statement' at, say, line 42:
 //
 //   for (int UNIQUE_INDEX = 0; UNIQUE_INDEX < 10; UNIQUE_INDEX++ )
 //
 // UNIQUE_INDEX will expand to index_42.  Since all three instances
 // are on the same line, they will have the same expansion.
 //
 // Note that multi-line CPP macros expand into just a single source
 // line.
 //

#define JOIN_(a,b)      a##b
#define INDEX_LABEL(l)  JOIN_(index_, l)
#define UNIQUE_INDEX    INDEX_LABEL(__LINE__)

#define LENGTH_LABEL(l) JOIN_(length_, l)
#define UNIQUE_LENGTH   LENGTH_LABEL(__LINE__)

 // --------------
 // Toml Iterators

#define FOR_EACH_KEY_IN_TABLE( tab, key ) \
    for (int UNIQUE_INDEX = 0;                                    \
         NULL     != ((key) = toml_key_in( (tab), UNIQUE_INDEX)); \
         UNIQUE_INDEX++                                           \
    )

#define FOR_EACH_ELEMENT_IN_ARRAY( array, i ) \
  const int UNIQUE_LENGTH = toml_array_nelem( array ); \
  for ( int i = 0; i < UNIQUE_LENGTH; i++)

  // Some elements may not be a Table, in which case tab value is NULL
#define FOR_EACH_TABLE_IN_ARRAY( array, tab ) \
  toml_table_t *(tab);                                                            \
  const int UNIQUE_LENGTH = toml_array_nelem((array));                            \
  for ( int UNIQUE_INDEX = 0;                                                     \
      (tab) = toml_table_at((array), UNIQUE_INDEX), UNIQUE_INDEX < UNIQUE_LENGTH; \
      UNIQUE_INDEX++                                                              \
  )

#define KEY_EQ_STRING(k,s) ( strncmp((k), (s), sizeof((s)) ) == 0 )

#endif // TOML_ITERATORS_H
